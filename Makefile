include .env

app-run:
	mkdir -p ./volumes/postgres
	docker-compose build ${APP_NAME}
	docker-compose up ${APP_NAME}
app-run-detach:
	mkdir -p ./volumes/postgres
	docker-compose build ${APP_NAME}
	docker-compose up ${APP_NAME} -d
app-remove:
	docker-compose down -v
	docker rmi wallet-${APP_NAME}
build:
	docker-compose build ${APP_NAME}
up:
	docker-compose up ${APP_NAME}
up-detach:
	docker-compose up -d
stop:
	docker-compose stop
down:
	docker-compose down -v
remove-api-img:
	docker rmi wallet-${APP_NAME}
ps:
	docker-compose ps
db-clean:
	sudo rm -rf ./volumes
create-net:
	docker network create universe
delete-net:
	docker network rm universe
swag-init:
	swag init -g internal/router/router.go
keys-generate:
	openssl genrsa -out ./tokenKeys/access-private.pem 2048 && openssl genrsa -out ./tokenKeys/refresh-private.pem 2048
keys-export:
	openssl rsa -in ./tokenKeys/access-private.pem -outform PEM -pubout -out ./tokenKeys/access-public.pem && \
    openssl rsa -in ./tokenKeys/refresh-private.pem -outform PEM -pubout -out ./tokenKeys/refresh-public.pem