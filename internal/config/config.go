package config

import (
	"fmt"
	"os"
	"reflect"
	"strconv"
	"time"
)

//go:generate easytags $GOFILE env,yaml

type AppConf struct {
	AppName string `env:"APP_NAME" yaml:"app_name"`
	DB      DB     `yaml:"db"`
	Server  Server `yaml:"server"`
	Logger  Logger `yaml:"logger"`
	Token   Token  `yaml:"token"`
}

type DB struct {
	Driver   string `env:"DB_DRIVER" yaml:"driver"`
	Name     string `env:"DB_NAME" yaml:"name"`
	User     string `env:"DB_USER" yaml:"user"`
	Password string `env:"DB_PWD" yaml:"password"`
	Host     string `env:"DB_HOST" yaml:"host"`
	Port     string `env:"DB_PORT" yaml:"port"`
	MaxConn  int    `env:"DB_CONN" yaml:"max_conn"`
	Timeout  int    `env:"DB_TIMEOUT" yaml:"timeout"`
}

type Server struct {
	Port            string        `env:"PORT" yaml:"port"`
	ShutdownTimeout time.Duration `env:"SHUTDOWN_TIMEOUT" yaml:"shutdown_timeout"`
}

type Logger struct {
	Level string `yaml:"level"`
}

type Token struct {
	AccessPrivateKeyPath  string        `env:"ACCESS_PRIVATE_KEY_PATH" yaml:"access_private_key_path"`
	AccessPublicKeyPath   string        `env:"ACCESS_PUBLIC_KEY_PATH" yaml:"access_public_key_path"`
	RefreshPrivateKeyPath string        `env:"REFRESH_PRIVATE_KEY_PATH" yaml:"refresh_private_key_path"`
	RefreshPublicKeyPath  string        `env:"REFRESH_PUBLIC_KEY_PATH" yaml:"refresh_public_key_path"`
	AccessTTL             time.Duration `env:"ACCESS_TTL" yaml:"access_ttl"`
	RefreshTTL            time.Duration `env:"REFRESH_TTL" yaml:"refresh_ttl"`
}

func NewAppConf() AppConf {
	var config AppConf
	err := confInit(&config)
	if err != nil {
		fmt.Printf("Error mapping env to struct: %v\n", err)
		return AppConf{}
	}

	return config
}

func confInit(config interface{}) error {
	configValue := reflect.ValueOf(config)

	if configValue.Kind() == reflect.Ptr {
		configValue = configValue.Elem()
	}

	if configValue.Kind() != reflect.Struct {
		return fmt.Errorf("input is not a struct")
	}

	configType := configValue.Type()

	for i := 0; i < configValue.NumField(); i++ {
		fieldValue := configValue.Field(i)
		fieldType := configType.Field(i)

		if fieldValue.Kind() == reflect.Struct {
			err := confInit(fieldValue.Addr().Interface())
			if err != nil {
				return err
			}
		} else {
			tagValue := fieldType.Tag.Get("env")
			if tagValue == "" {
				continue
			}
			envValue := os.Getenv(tagValue)
			if envValue == "" {
				continue
			}

			switch fieldValue.Kind() {
			case reflect.String:
				fieldValue.SetString(envValue)
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				if intValue, err := strconv.ParseInt(envValue, 10, 64); err == nil {
					fieldValue.SetInt(intValue)
				} else {
					return fmt.Errorf("error parsing int value for field %s: %v", fieldType.Name, err)
				}
			default:
				return fmt.Errorf("unsupported field type for field %s", fieldType.Name)
			}
		}
	}

	return nil
}
