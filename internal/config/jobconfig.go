package config

import (
	"fmt"
	"time"
)

//go:generate easytags $GOFILE env,yaml

type JobConf struct {
	JobName        string         `env:"JOB_NAME" yaml:"job_name"`
	Logger         Logger         `yaml:"logger"`
	DB             DB             `yaml:"db"`
	CurrencyWorker CurrencyWorker `yaml:"currency_worker"`
}

type CurrencyWorker struct {
	CurrencySource string        `env:"CURRENCY_SOURCE" yaml:"currency_source"`
	CurrencyTicker time.Duration `env:"CURRENCY_TICKER" yaml:"ticker_time" `
}

func NewJobConf() JobConf {
	var config JobConf
	err := confInit(&config)
	if err != nil {
		fmt.Printf("mapping env to struct: %v\n", err)
		return JobConf{}
	}

	return config
}
