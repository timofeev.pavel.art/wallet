package pgsql

import (
	"context"
	"database/sql"
	"time"

	"gitlab.com/timofeev.pavel.art/wallet/internal/storage"

	"github.com/jmoiron/sqlx"
	"gitlab.com/timofeev.pavel.art/wallet/internal/models"
	"go.uber.org/zap"
)

type PostgresStorage struct {
	db  *sqlx.DB
	log *zap.Logger
}

func NewPostgresStorage(db *sqlx.DB, log *zap.Logger) *PostgresStorage {
	return &PostgresStorage{db: db, log: log}
}

func (s *PostgresStorage) CreateUser(ctx context.Context, user models.User) error {
	tx, err := s.db.BeginTx(ctx, &sql.TxOptions{Isolation: 4})
	if err != nil {
		return err
	}

	query := `
				INSERT INTO users (first_name, last_name, email, password, token_hash, id_currency, created_at, updated_at)
				VALUES ($1, $2, $3, $4, $5, $6, $7, $8);
	`

	if _, err = tx.ExecContext(ctx, query,
		user.FirstName,
		user.LastName,
		user.Email,
		user.Password,
		user.TokenHash,
		user.CurrencyID,
		time.Now(), time.Now()); err != nil {
		_ = tx.Rollback()
		return err
	}

	return tx.Commit()
}

func (s *PostgresStorage) GetUserByID(ctx context.Context, userID string) (models.User, error) {
	var user models.User
	query := `SELECT  *FROM users WHERE id_user = $1`

	if err := s.db.GetContext(ctx, &user, query, userID); err != nil {
		return models.User{}, err
	}

	return user, nil
}

func (s *PostgresStorage) GetUserByEmail(ctx context.Context, email string) (models.User, error) {
	var user models.User
	query := `SELECT * FROM users WHERE email = $1`

	if err := s.db.GetContext(ctx, &user, query, email); err != nil {
		return models.User{}, err
	}

	return user, nil
}

// UpsertExchanges method insert new records or update its, if its already exists.
// Query explain:
// Insert into target table
// Select for forming inserting data into exchange table
func (s *PostgresStorage) UpsertExchanges(ctx context.Context, exchangeList []models.Exchange) error {
	tx, err := s.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		_ = tx.Rollback()
		return err
	}
	query := `
				INSERT INTO exchange (id_source, id_target, integer, fraction, updated_at)
				SELECT
					source.id_currency AS id_source,
					target.id_currency AS id_target,
						$1, 
						$2, 
						$3 
				FROM currency AS source
					JOIN currency AS target ON source.name = $4 AND target.name = $5
				ON CONFLICT (id_source, id_target)
				DO UPDATE SET
				integer = EXCLUDED.integer,
				fraction = EXCLUDED.fraction,
				updated_at = EXCLUDED.updated_at;		
	`

	for _, ex := range exchangeList {
		_, err = tx.ExecContext(ctx, query,
			ex.Integer,
			ex.Fraction,
			time.Now(),
			ex.Source,
			ex.Target)
		if err != nil {
			_ = tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}

func (s *PostgresStorage) CreateCurrencyList(ctx context.Context, currencies []models.Currency) error {
	query := `INSERT INTO currency (name) VALUES(:name) ON CONFLICT DO NOTHING `

	if _, err := s.db.NamedExecContext(ctx, query, currencies); err != nil {
		return err
	}

	return nil
}

func (s *PostgresStorage) CreateWallet(ctx context.Context, wallet models.Wallet) error {
	query := `INSERT INTO wallet (name, id_user) VALUES(:name, :id_user)`

	if _, err := s.db.NamedExecContext(ctx, query, wallet); err != nil {
		return err
	}

	return nil
}

func (s *PostgresStorage) UpdateWalletByID(ctx context.Context, id int, wallet models.Wallet) error {
	query := `UPDATE wallet SET name=$1 WHERE id_wallet=$2`

	if _, err := s.db.ExecContext(ctx, query, wallet.Name, id); err != nil {
		return err
	}

	return nil
}

func (s *PostgresStorage) GetWalletByID(ctx context.Context, walletID int) (models.Wallet, error) {
	var wallet []models.Wallet
	query := `SELECT * FROM wallet WHERE id_wallet=$1 LIMIT 1`

	if err := s.db.SelectContext(ctx, &wallet, query, walletID); err != nil {
		return models.Wallet{}, err
	}

	if len(wallet) == 0 {
		return models.Wallet{}, storage.ErrWalletDoesNotExist
	}

	return wallet[0], nil
}

func (s *PostgresStorage) GetWalletListByUserID(ctx context.Context, userID string) ([]models.Wallet, error) {
	var wallets []models.Wallet
	query := `SELECT * FROM wallet WHERE id_user=$1`

	if err := s.db.SelectContext(ctx, &wallets, query, userID); err != nil {
		return nil, err
	}

	return wallets, nil
}

func (s *PostgresStorage) DeleteWalletByID(ctx context.Context, walletID int) error {
	query := `DELETE FROM wallet WHERE id_wallet=$1`

	if _, err := s.db.ExecContext(ctx, query, walletID); err != nil {
		return err
	}

	return nil
}

func (s *PostgresStorage) CreateCategory(ctx context.Context, category models.Category) error {
	query := `INSERT INTO category (name) VALUES(:name)`

	if _, err := s.db.NamedExecContext(ctx, query, category); err != nil {
		return err
	}

	return nil
}

func (s *PostgresStorage) UpdateCategoryByID(ctx context.Context, id int, category models.Category) error {
	query := `UPDATE category SET name=$1 WHERE id_category=$2`

	if _, err := s.db.ExecContext(ctx, query, category.Name, id); err != nil {
		return err
	}

	return nil
}

func (s *PostgresStorage) GetCategoryByID(ctx context.Context, categoryID int) (models.Category, error) {
	var category []models.Category
	query := `SELECT * FROM category WHERE id_category=$1 LIMIT 1`

	if err := s.db.SelectContext(ctx, &category, query, categoryID); err != nil {
		return models.Category{}, err
	}

	if len(category) == 0 {
		return models.Category{}, storage.ErrCategoryDoesNotExist
	}

	return category[0], nil
}

func (s *PostgresStorage) GetCategoryList(ctx context.Context) ([]models.Category, error) {
	var categories []models.Category
	query := `SELECT * FROM category`

	if err := s.db.SelectContext(ctx, &categories, query); err != nil {
		return nil, err
	}

	return categories, nil
}

func (s *PostgresStorage) DeleteCategoryByID(ctx context.Context, categoryID int) error {
	query := `DELETE FROM category WHERE id_category=$1`

	if _, err := s.db.ExecContext(ctx, query, categoryID); err != nil {
		return err
	}

	return nil
}
