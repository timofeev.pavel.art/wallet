package storage

import (
	"context"
	"errors"

	"gitlab.com/timofeev.pavel.art/wallet/internal/models"
)

var (
	ErrWalletDoesNotExist   = errors.New("wallet does not exist")
	ErrCategoryDoesNotExist = errors.New("category does not exist")
)

type Storage interface {
	CreateUser(ctx context.Context, user models.User) error
	GetUserByID(ctx context.Context, userID string) (models.User, error)
	GetUserByEmail(ctx context.Context, email string) (models.User, error)
	CreateCurrencyList(ctx context.Context, currencies []models.Currency) error
	UpsertExchanges(ctx context.Context, exchange []models.Exchange) error

	CreateWallet(ctx context.Context, wallet models.Wallet) error
	UpdateWalletByID(ctx context.Context, id int, wallet models.Wallet) error
	GetWalletByID(ctx context.Context, walletID int) (models.Wallet, error)
	GetWalletListByUserID(ctx context.Context, userID string) ([]models.Wallet, error)
	DeleteWalletByID(ctx context.Context, walletID int) error

	CreateCategory(ctx context.Context, category models.Category) error
	UpdateCategoryByID(ctx context.Context, id int, category models.Category) error
	GetCategoryByID(ctx context.Context, categoryID int) (models.Category, error)
	GetCategoryList(ctx context.Context) ([]models.Category, error)
	DeleteCategoryByID(ctx context.Context, categoryID int) error
}
