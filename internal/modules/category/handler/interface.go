package handler

import (
	"context"
	"errors"
	"net/http"

	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/responder"

	"gitlab.com/timofeev.pavel.art/wallet/internal/modules/category/service"
)

var (
	ErrInvalidData       = errors.New("invalid input data")
	ErrEmptyCategoryName = errors.New("empty category name")
	ErrCategoryNotFound  = errors.New("category does not exist")
)

type Categorier interface {
	Create(w http.ResponseWriter, r *http.Request)
	GetList(w http.ResponseWriter, r *http.Request)
	Edit(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	CategoryCtx(next http.Handler) http.Handler
}

type CategoryService interface {
	CreateCategory(ctx context.Context, in service.CreateCategoryIn) service.CreateCategoryOut
	EditCategory(ctx context.Context, in service.EditCategoryIn) service.EditCategoryOut
	GetCategoryById(ctx context.Context, in service.GetCategoryByIdIn) service.GetCategoryByIdOut
	GetCategoryList(ctx context.Context, in service.GetCategoryListIn) service.GetCategoryListOut
	DeleteCategory(ctx context.Context, in service.DeleteCategoryIn) service.DeleteCategoryOut
}

type CategoryHandler struct {
	categoryService CategoryService
	responder.Responder
}

func NewCategoryHandler(cService CategoryService, respond responder.Responder) *CategoryHandler {
	return &CategoryHandler{
		categoryService: cService,
		Responder:       respond,
	}
}
