package handler

import "gitlab.com/timofeev.pavel.art/wallet/internal/models"

//go:generate easytags $GOFILE json
type CreateCategoryRequest struct {
	Name string `json:"name"`
}

type GetCategoryListResponse struct {
	Categories []models.Category `json:"categories"`
}

type EditCategoryRequest struct {
	Name string `json:"name"`
}

type DeleteCategoryRequest struct {
	CategoryId int `json:"category_id"`
}

type UserCategoryRequest struct{}
