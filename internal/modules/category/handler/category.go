package handler

import (
	"encoding/json"
	"net/http"
	"strings"

	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/responder"
	"gitlab.com/timofeev.pavel.art/wallet/internal/models"
	"gitlab.com/timofeev.pavel.art/wallet/internal/modules/category/service"
)

// Create godoc
// @Summary Create new category
// @Description Create new category from request data
// @Tags category
// @Accept json
// @Produce json
// @Param data body CreateCategoryRequest true "Category data"
// @Success 200 {object} responder.Response
// @Failure 400 {object} responder.Response
// @Router /category/create [post]
// @Security Bearer
func (c *CategoryHandler) Create(w http.ResponseWriter, r *http.Request) {
	var req CreateCategoryRequest
	var err error

	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		c.ErrorBadRequest(w, ErrInvalidData)
		return
	}

	if err = checkCategoryName(req.Name); err != nil {
		c.ErrorBadRequest(w, err)
		return
	}

	out := c.categoryService.CreateCategory(r.Context(), service.CreateCategoryIn{Category: models.Category{
		Name: req.Name,
	}})

	if out.Err != nil {
		c.ErrorBadRequest(w, out.Err)
		return
	}

	c.OutputJSON(w, responder.Response{
		Success: true,
		Message: "Category created successfully!",
	})
}

func checkCategoryName(name string) error {
	strip := strings.ReplaceAll(name, " ", "")

	if len(strip) == 0 {
		return ErrEmptyCategoryName
	}

	return nil
}

// GetList godoc
// @Summary GetList category
// @Description GetList category by id
// @Tags category
// @Produce json
// @Success 200 {object} responder.Response
// @Failure 400 {object} responder.Response
// @Router /categories [get]
func (c *CategoryHandler) GetList(w http.ResponseWriter, r *http.Request) {
	out := c.categoryService.GetCategoryList(r.Context(), service.GetCategoryListIn{})

	if out.Err != nil {
		c.ErrorBadRequest(w, out.Err)
		return
	}

	c.OutputJSON(w, responder.Response{
		Success: true,
		Message: "Get all categories",
		Data:    GetCategoryListResponse{Categories: out.Categories},
	})
}

// Edit godoc
// @Summary Edit category
// @Description Edit category by id
// @Tags category
// @Accept json
// @Produce json
// @Param data body EditCategoryRequest true "Category data"
// @Success 200 {object} responder.Response
// @Failure 400 {object} responder.Response
// @Router /category/{categoryId}/edit [patch]
// @Security Bearer
func (c *CategoryHandler) Edit(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	category, ok := ctx.Value(UserCategoryRequest{}).(models.Category)

	if !ok {
		c.ErrorBadRequest(w, ErrEmptyCategoryName)
		return
	}

	var req EditCategoryRequest

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		c.ErrorBadRequest(w, ErrInvalidData)
		return
	}

	if err := checkCategoryName(req.Name); err != nil {
		c.ErrorBadRequest(w, ErrEmptyCategoryName)
		return
	}

	out := c.categoryService.EditCategory(r.Context(), service.EditCategoryIn{
		CategoryID: category.ID,
		Name:       req.Name,
	})

	if out.Err != nil {
		c.ErrorBadRequest(w, out.Err)
		return
	}

	c.OutputJSON(w, responder.Response{
		Success: true,
		Message: "Category updated successfully!",
	})
}

// Delete godoc
// @Summary Delete category
// @Description Delete category by id
// @Tags category
// @Accept json
// @Produce json
// @Param data body DeleteCategoryRequest true "Category data"
// @Success 200 {object} responder.Response
// @Failure 400 {object} responder.Response
// @Router /category/{categoryId}/delete [delete]
// @Security Bearer
func (c *CategoryHandler) Delete(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	category, ok := ctx.Value(UserCategoryRequest{}).(models.Category)

	if !ok {
		c.ErrorBadRequest(w, ErrEmptyCategoryName)
		return
	}

	out := c.categoryService.DeleteCategory(r.Context(), service.DeleteCategoryIn{
		CategoryID: category.ID,
	})

	if out.Err != nil {
		c.ErrorBadRequest(w, out.Err)
		return
	}

	c.OutputJSON(w, responder.Response{
		Success: true,
		Message: "Category deleted successfully!",
	})
}
