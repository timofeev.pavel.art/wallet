package handler

import (
	"context"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"gitlab.com/timofeev.pavel.art/wallet/internal/modules/category/service"
)

func (c *CategoryHandler) CategoryCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var out service.GetCategoryByIdOut

		if categoryID := chi.URLParam(r, "categoryId"); categoryID != "" {
			id, _ := strconv.Atoi(categoryID)
			out = c.categoryService.GetCategoryById(r.Context(), service.GetCategoryByIdIn{CategoryID: id})
		} else {
			c.ErrorNotFound(w, ErrCategoryNotFound)
			return
		}

		if out.Err != nil {
			c.ErrorBadRequest(w, out.Err)
			return
		}

		ctx := context.WithValue(r.Context(), UserCategoryRequest{}, out.Category)
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}
