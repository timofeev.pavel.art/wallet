package service

import "gitlab.com/timofeev.pavel.art/wallet/internal/models"

type CreateCategoryIn struct {
	Category models.Category
}

type CreateCategoryOut struct {
	Err error
}

type EditCategoryIn struct {
	CategoryID int
	Name       string
}

type EditCategoryOut struct {
	Err error
}

type GetCategoryListIn struct {
}

type GetCategoryListOut struct {
	Categories []models.Category
	Err        error
}

type GetCategoryByIdIn struct {
	CategoryID int
}

type GetCategoryByIdOut struct {
	Category models.Category
	Err      error
}

type DeleteCategoryIn struct {
	CategoryID int
}

type DeleteCategoryOut struct {
	Err error
}
