package service

import (
	"context"
	"errors"

	"github.com/lib/pq"
	"gitlab.com/timofeev.pavel.art/wallet/internal/models"
	"gitlab.com/timofeev.pavel.art/wallet/internal/storage"
	"go.uber.org/zap"
)

// CreateCategory method create new category
func (c *CategoryService) CreateCategory(ctx context.Context, in CreateCategoryIn) CreateCategoryOut {
	err := c.categoryStorage.CreateCategory(ctx, in.Category)

	if err != nil {
		c.log.Error("create category error", zap.Error(err))
		return CreateCategoryOut{Err: ErrCreateCategory}
	}

	return CreateCategoryOut{}
}

// EditCategory method update category info by id
func (c *CategoryService) EditCategory(ctx context.Context, in EditCategoryIn) EditCategoryOut {
	err := c.categoryStorage.UpdateCategoryByID(ctx, in.CategoryID, models.Category{Name: in.Name})
	if err != nil {
		var v pq.Error
		if errors.As(err, &v) && v.Code == "23503" {
			return EditCategoryOut{Err: ErrCategoryDoesNotExist}
		}

		c.log.Error("get category by id error", zap.Error(err))
		return EditCategoryOut{Err: ErrUpdateCategory}
	}

	return EditCategoryOut{}
}

// GetCategoryById method return category by id. If category does not exist, return error
func (c *CategoryService) GetCategoryById(ctx context.Context, in GetCategoryByIdIn) GetCategoryByIdOut {
	category, err := c.categoryStorage.GetCategoryByID(ctx, in.CategoryID)
	if err != nil {
		var v pq.Error
		if errors.Is(err, storage.ErrCategoryDoesNotExist) || errors.As(err, &v) && v.Code == "23503" {
			return GetCategoryByIdOut{Err: ErrCategoryDoesNotExist}
		}

		c.log.Error("update category by id error", zap.Error(err))
		return GetCategoryByIdOut{Err: ErrGetCategory}
	}

	return GetCategoryByIdOut{Category: category}
}

// GetCategoryList method get all categories from repository
func (c *CategoryService) GetCategoryList(ctx context.Context, in GetCategoryListIn) GetCategoryListOut {
	categories, err := c.categoryStorage.GetCategoryList(ctx)
	if err != nil {
		c.log.Error("get category list error", zap.Error(err))
		return GetCategoryListOut{Err: ErrGetCategory}
	}

	return GetCategoryListOut{Categories: categories}
}

// DeleteCategory method delete category by id
func (c *CategoryService) DeleteCategory(ctx context.Context, in DeleteCategoryIn) DeleteCategoryOut {
	err := c.categoryStorage.DeleteCategoryByID(ctx, in.CategoryID)

	if err != nil {
		var v pq.Error
		if errors.As(err, &v) && v.Code == "23503" {
			return DeleteCategoryOut{Err: ErrCategoryDoesNotExist}
		}

		c.log.Error("delete category error", zap.Error(err))
		return DeleteCategoryOut{Err: ErrDeleteCategory}
	}

	return DeleteCategoryOut{}
}
