package service

import (
	"context"
	"errors"

	"gitlab.com/timofeev.pavel.art/wallet/internal/models"
	"go.uber.org/zap"
)

var (
	ErrCreateCategory       = errors.New("create category error")
	ErrUpdateCategory       = errors.New("update category error")
	ErrCategoryDoesNotExist = errors.New("category does not exist")
	ErrDeleteCategory       = errors.New("delete category error")
	ErrGetCategory          = errors.New("get category error")
)

type Categorier interface {
	CreateCategory(ctx context.Context, in CreateCategoryIn) CreateCategoryOut
	EditCategory(ctx context.Context, in EditCategoryIn) EditCategoryOut
	GetCategoryById(ctx context.Context, in GetCategoryByIdIn) GetCategoryByIdOut
	GetCategoryList(ctx context.Context, in GetCategoryListIn) GetCategoryListOut
	DeleteCategory(ctx context.Context, in DeleteCategoryIn) DeleteCategoryOut
}

//go:generate mockery --name CategoryProvider
type CategoryProvider interface {
	CreateCategory(ctx context.Context, category models.Category) error
	UpdateCategoryByID(ctx context.Context, id int, category models.Category) error
	GetCategoryByID(ctx context.Context, categoryID int) (models.Category, error)
	GetCategoryList(ctx context.Context) ([]models.Category, error)
	DeleteCategoryByID(ctx context.Context, categoryID int) error
}

type CategoryService struct {
	categoryStorage CategoryProvider
	log             *zap.Logger
}

func NewCategoryService(cStorage CategoryProvider, log *zap.Logger) *CategoryService {
	return &CategoryService{categoryStorage: cStorage, log: log}
}
