package service

import (
	"context"
	"errors"
	"os"
	"testing"

	"gitlab.com/timofeev.pavel.art/wallet/internal/models"

	"gitlab.com/timofeev.pavel.art/wallet/internal/storage"

	"github.com/lib/pq"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/logs"
	"gitlab.com/timofeev.pavel.art/wallet/internal/modules/category/service/mocks"
)

type CategoryServiceSuite struct {
	suite.Suite
	*require.Assertions

	categoryStorage *mocks.CategoryProvider

	categoryService Categorier
}

func TestCategoryServiceSuite(t *testing.T) {
	suite.Run(t, new(CategoryServiceSuite))
}

func (w *CategoryServiceSuite) SetupTest() {
	w.Assertions = require.New(w.T())

	w.categoryStorage = mocks.NewCategoryProvider(w.T())

	logger := logs.NewLogger("debug", os.Stdout)
	w.categoryService = NewCategoryService(w.categoryStorage, logger)
}

func (w *CategoryServiceSuite) TestCategoryService_CreateCategory() {
	tests := []struct {
		name   string
		arg    CreateCategoryIn
		expect CreateCategoryOut
		on     func(storage *mocks.CategoryProvider)
	}{
		{
			name:   "HappyPath",
			arg:    CreateCategoryIn{models.Category{Name: "example"}},
			expect: CreateCategoryOut{Err: nil},
			on: func(storage *mocks.CategoryProvider) {
				storage.On("CreateCategory", mock.Anything, mock.AnythingOfType("models.Category")).
					Return(nil).Once()
			},
		},
		{
			name:   "Error create category",
			arg:    CreateCategoryIn{models.Category{Name: "example"}},
			expect: CreateCategoryOut{Err: ErrCreateCategory},
			on: func(s *mocks.CategoryProvider) {
				s.On("CreateCategory", mock.Anything, mock.AnythingOfType("models.Category")).
					Return(errors.New("some error")).Once()
			},
		},
	}
	for _, tt := range tests {
		w.Run(tt.name, func() {
			if tt.on != nil {
				tt.on(w.categoryStorage)
			}

			out := w.categoryService.CreateCategory(context.Background(), tt.arg)

			assert.Equal(w.T(), tt.expect, out)
		})
	}
}

func (w *CategoryServiceSuite) TestCategoryService_DeleteCategory() {
	tests := []struct {
		name   string
		arg    DeleteCategoryIn
		expect DeleteCategoryOut
		on     func(storage *mocks.CategoryProvider)
	}{
		{
			name:   "HappyPath",
			arg:    DeleteCategoryIn{CategoryID: 1},
			expect: DeleteCategoryOut{Err: nil},
			on: func(s *mocks.CategoryProvider) {
				s.On("DeleteCategoryByID", mock.Anything, mock.AnythingOfType("int")).
					Return(nil).Once()
			},
		},
		{
			name:   "pq error 23503, category does not exist",
			arg:    DeleteCategoryIn{CategoryID: 1},
			expect: DeleteCategoryOut{Err: ErrCategoryDoesNotExist},
			on: func(s *mocks.CategoryProvider) {
				s.On("DeleteCategoryByID", mock.Anything, mock.AnythingOfType("int")).
					Return(pq.Error{Code: "23503"}).Once()
			},
		},
		{
			name:   "delete category error",
			arg:    DeleteCategoryIn{CategoryID: 1},
			expect: DeleteCategoryOut{Err: ErrDeleteCategory},
			on: func(s *mocks.CategoryProvider) {
				s.On("DeleteCategoryByID", mock.Anything, mock.AnythingOfType("int")).
					Return(errors.New("some error")).Once()
			},
		},
	}
	for _, tt := range tests {
		w.Run(tt.name, func() {
			if tt.on != nil {
				tt.on(w.categoryStorage)
			}

			out := w.categoryService.DeleteCategory(context.Background(), tt.arg)

			assert.Equal(w.T(), tt.expect, out)
		})
	}
}

func (w *CategoryServiceSuite) TestCategoryService_EditCategory() {
	tests := []struct {
		name   string
		arg    EditCategoryIn
		expect EditCategoryOut
		on     func(storage *mocks.CategoryProvider)
	}{
		{
			name:   "HappyPath",
			arg:    EditCategoryIn{CategoryID: 1, Name: "example1"},
			expect: EditCategoryOut{Err: nil},
			on: func(s *mocks.CategoryProvider) {
				s.On("UpdateCategoryByID", mock.Anything, mock.AnythingOfType("int"), mock.AnythingOfType("models.Category")).
					Return(nil).Once()
			},
		},
		{
			name:   "pq error 23503, category does not exist",
			arg:    EditCategoryIn{CategoryID: 1, Name: "example1"},
			expect: EditCategoryOut{Err: ErrCategoryDoesNotExist},
			on: func(s *mocks.CategoryProvider) {
				s.On("UpdateCategoryByID", mock.Anything, mock.AnythingOfType("int"), mock.AnythingOfType("models.Category")).
					Return(pq.Error{Code: "23503"}).Once()
			},
		},
		{
			name:   "update category error",
			arg:    EditCategoryIn{CategoryID: 1, Name: "example1"},
			expect: EditCategoryOut{Err: ErrUpdateCategory},
			on: func(s *mocks.CategoryProvider) {
				s.On("UpdateCategoryByID", mock.Anything, mock.AnythingOfType("int"), mock.AnythingOfType("models.Category")).
					Return(errors.New("some error")).Once()
			},
		},
	}
	for _, tt := range tests {
		w.Run(tt.name, func() {
			if tt.on != nil {
				tt.on(w.categoryStorage)
			}

			out := w.categoryService.EditCategory(context.Background(), tt.arg)

			assert.Equal(w.T(), tt.expect, out)
		})
	}
}

func (w *CategoryServiceSuite) TestCategoryService_GetCategoryById() {
	tests := []struct {
		name   string
		arg    GetCategoryByIdIn
		expect GetCategoryByIdOut
		on     func(storage *mocks.CategoryProvider)
	}{
		{
			name: "HappyPath",
			arg:  GetCategoryByIdIn{CategoryID: 1},
			expect: GetCategoryByIdOut{Category: models.Category{
				ID:   1,
				Name: "example",
			}, Err: nil},
			on: func(s *mocks.CategoryProvider) {
				s.On("GetCategoryByID", mock.Anything, mock.AnythingOfType("int")).
					Return(models.Category{
						ID:   1,
						Name: "example",
					}, nil).Once()
			},
		},
		{
			name:   "category does not exist",
			arg:    GetCategoryByIdIn{CategoryID: 1},
			expect: GetCategoryByIdOut{Err: ErrCategoryDoesNotExist},
			on: func(s *mocks.CategoryProvider) {
				s.On("GetCategoryByID", mock.Anything, mock.AnythingOfType("int")).
					Return(models.Category{}, storage.ErrCategoryDoesNotExist).Once()
			},
		},
		{
			name:   "pq error 23503, category does not exist",
			arg:    GetCategoryByIdIn{CategoryID: 1},
			expect: GetCategoryByIdOut{Err: ErrCategoryDoesNotExist},
			on: func(s *mocks.CategoryProvider) {
				s.On("GetCategoryByID", mock.Anything, mock.AnythingOfType("int")).
					Return(models.Category{}, pq.Error{Code: "23503"}).Once()
			},
		},
		{
			name:   "get category error",
			arg:    GetCategoryByIdIn{CategoryID: 1},
			expect: GetCategoryByIdOut{Err: ErrGetCategory},
			on: func(s *mocks.CategoryProvider) {
				s.On("GetCategoryByID", mock.Anything, mock.AnythingOfType("int")).
					Return(models.Category{}, errors.New("some error")).Once()
			},
		},
	}
	for _, tt := range tests {
		w.Run(tt.name, func() {
			if tt.on != nil {
				tt.on(w.categoryStorage)
			}

			out := w.categoryService.GetCategoryById(context.Background(), tt.arg)

			assert.Equal(w.T(), tt.expect, out)
		})
	}
}

func (w *CategoryServiceSuite) TestCategoryService_GetCategoryList() {
	tests := []struct {
		name   string
		arg    GetCategoryListIn
		expect GetCategoryListOut
		on     func(storage *mocks.CategoryProvider)
	}{
		{
			name: "HappyPath",
			arg:  GetCategoryListIn{},
			expect: GetCategoryListOut{Categories: []models.Category{
				{
					ID:   1,
					Name: "example1",
				},
				{
					ID:   2,
					Name: "example2",
				}}, Err: nil},
			on: func(s *mocks.CategoryProvider) {
				s.On("GetCategoryList", mock.Anything).
					Return([]models.Category{
						{
							ID:   1,
							Name: "example1",
						},
						{
							ID:   2,
							Name: "example2",
						}}, nil).Once()
			},
		},
		{
			name:   "get category error",
			arg:    GetCategoryListIn{},
			expect: GetCategoryListOut{Err: ErrGetCategory},
			on: func(s *mocks.CategoryProvider) {
				s.On("GetCategoryList", mock.Anything).
					Return(nil, errors.New("some error")).Once()
			},
		},
	}
	for _, tt := range tests {
		w.Run(tt.name, func() {
			if tt.on != nil {
				tt.on(w.categoryStorage)
			}

			out := w.categoryService.GetCategoryList(context.Background(), tt.arg)

			assert.Equal(w.T(), tt.expect, out)
		})
	}
}
