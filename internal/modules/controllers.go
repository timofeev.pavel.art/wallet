package modules

import (
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/responder"
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/token"
	authHand "gitlab.com/timofeev.pavel.art/wallet/internal/modules/auth/handler"
	categoryHand "gitlab.com/timofeev.pavel.art/wallet/internal/modules/category/handler"
	walletHand "gitlab.com/timofeev.pavel.art/wallet/internal/modules/wallet/handler"
)

type Controllers struct {
	AuthHandler     authHand.Auther
	WalletHandler   walletHand.Walleter
	CategoryHandler categoryHand.Categorier
}

func NewControllers(services *Services, resp responder.Responder, tokenMgr token.TokenManager) *Controllers {
	return &Controllers{
		AuthHandler:     authHand.NewAuthHandler(resp, tokenMgr, services.AuthService),
		WalletHandler:   walletHand.NewWalletHandler(resp, services.WalletService, tokenMgr),
		CategoryHandler: categoryHand.NewCategoryHandler(services.CategoryService, resp),
	}
}
