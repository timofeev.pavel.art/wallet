package handler

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/mail"
	"unicode"

	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/token"
	"gitlab.com/timofeev.pavel.art/wallet/internal/modules/auth/service"

	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/responder"
)

// Register godoc
// @Summary      Register new user account
// @Description  Create new user from request data
// @Tags         authenticate
// @Accept       json
// @Produce      json
// @Param        data   body      RegisterRequest  true  "User account data"
// @Success      200  {object}  responder.Response
// @Failure      400  {object}  responder.Response
// @Failure      500  {object}  responder.Response
// @Router       /register [post]
func (h *AuthHandler) Register(w http.ResponseWriter, r *http.Request) {
	var req RegisterRequest

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		h.ErrorBadRequest(w, err)
		return
	}

	// Check email has example@gmail.com structure
	if err := validateEmail(req.Email); err != nil {
		h.ErrorBadRequest(w, err)
		return
	}

	// Compare password with retype password
	if req.Password != req.RetypePassword {
		h.ErrorBadRequest(w, ErrMismatchPassword)
		return
	}

	// Check password for following requirements:
	// 1. >= 5 char
	// 2. There is at least one number, lower & upper letter and symbol each
	// 3. No repeating character sequences
	if err := validatePassword(req.Password); err != nil {
		h.ErrorBadRequest(w, err)
		return
	}

	if out := h.AuthService.UserRegister(r.Context(), service.UserRegisterIn{
		Email:      req.Email,
		Password:   req.Password,
		FirstName:  req.FirstName,
		LastName:   req.LastName,
		CurrencyID: req.CurrencyID,
	}); out.Err != nil {
		if errors.Is(out.Err, service.ErrUserAlreadyExists) {
			h.ErrorBadRequest(w, out.Err)
		} else {
			h.ErrorInternal(w, out.Err)
		}
		return
	}

	h.OutputJSON(w, responder.Response{
		Success: true,
		Message: "User created successfully!",
	})
}

func validatePassword(password string) error {
	var (
		runePwd                = []rune(password)
		ln                     = len(runePwd)
		hasLet, hasDig, hasSym bool
	)

	if len(runePwd) < 5 {
		return ErrShortPassword
	}

	for i := 0; i < ln; i++ {
		item := runePwd[i]
		if i < ln-1 && item == runePwd[i+1] {
			return ErrRepeatChar
		}

		switch {
		case !hasLet && unicode.IsLetter(item):
			hasLet = true
		case !hasDig && unicode.IsDigit(item):
			hasDig = true
		case !hasSym && (unicode.IsPunct(item) || unicode.IsSymbol(item)):
			hasSym = true
		}
	}

	if !hasLet || !hasDig || !hasSym {
		return ErrHasLetDigSymb
	}

	return nil
}

func validateEmail(email string) error {
	_, err := mail.ParseAddress(email)
	if err != nil || len(email) < 5 {
		return ErrInvalidEmail
	}
	return nil
}

// Login godoc
// @Summary      Login user to system
// @Description  Get email and password and return access & refresh token
// @Tags         authenticate
// @Accept       json
// @Produce      json
// @Param        data   body      LoginRequest  true  "User email and password, return tokens"
// @Success      200  {object}  responder.Response
// @Failure      400  {object}  responder.Response
// @Failure      500  {object}  responder.Response
// @Router       /login [post]
func (h *AuthHandler) Login(w http.ResponseWriter, r *http.Request) {
	var req LoginRequest

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		h.ErrorBadRequest(w, err)
		return
	}

	// Check email has example@gmail.com structure or password too short
	if err := validateEmail(req.Email); err != nil || len(req.Password) < 5 {
		h.ErrorBadRequest(w, ErrLoginDataInvalid)
		return
	}

	// Generate refresh & access token. If method return token generate error, then return internal error
	out := h.AuthService.VerifyLoginData(
		r.Context(),
		service.VerifyLoginDataIn{Email: req.Email, Password: req.Password},
	)
	if out.Err != nil {
		var tokenErr *token.ErrTokenGenerate
		if errors.As(out.Err, &tokenErr) {
			h.ErrorInternal(w, out.Err)
			return
		}

		h.ErrorBadRequest(w, ErrLoginDataInvalid)
		return
	}

	h.OutputJSON(w, responder.Response{
		Success: true,
		Message: "User login successfully!",
		Data: LoginResponse{
			RefreshToken: out.RefreshToken,
			AccessToken:  out.AccessToken,
			Email:        req.Email,
		},
	})
}

// RefreshToken godoc
// @Summary      RefreshToken generate new access token
// @Description  Extract user from refresh token and get new access token
// @Tags         authenticate
// @Accept       json
// @Produce      json
// @Success      200  {object}  responder.Response
// @Failure      400  {object}  responder.Response
// @Failure      500  {object}  responder.Response
// @Router       /refresh_token [get]
// @Security Bearer
func (h *AuthHandler) RefreshToken(w http.ResponseWriter, r *http.Request) {
	userID, err := h.tokenService.ExtractUserFromRefreshToken(r)
	if err != nil {
		h.ErrorBadRequest(w, err)
		return
	}

	out := h.AuthService.UserRefreshToken(r.Context(), service.UserRefreshTokenIn{UserID: userID})
	if out.Err != nil {
		h.ErrorBadRequest(w, out.Err)
		return
	}

	h.OutputJSON(w, responder.Response{
		Success: true,
		Message: "User refresh token successfully!",
		Data: RefreshResponse{
			AccessToken: out.AccessToken,
		},
	})
}
