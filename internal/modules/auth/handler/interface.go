package handler

import (
	"context"
	"errors"
	"net/http"

	"gitlab.com/timofeev.pavel.art/wallet/internal/modules/auth/service"

	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/responder"
)

var (
	ErrInvalidEmail     = errors.New("invalid email")
	ErrShortPassword    = errors.New("too short password")
	ErrRepeatChar       = errors.New("password has repeating characters sequence")
	ErrHasLetDigSymb    = errors.New("password must contain at least one symbol, number and letter of each")
	ErrMismatchPassword = errors.New("passwords mismatch")
	ErrLoginDataInvalid = errors.New("email or password is invalid")
)

type Auther interface {
	Register(w http.ResponseWriter, r *http.Request)
	Login(w http.ResponseWriter, r *http.Request)
	RefreshToken(w http.ResponseWriter, r *http.Request)
}

type tokenService interface {
	ExtractUserFromRefreshToken(r *http.Request) (string, error)
}

type AuthService interface {
	UserRegister(ctx context.Context, in service.UserRegisterIn) service.UserRegisterOut
	VerifyLoginData(ctx context.Context, in service.VerifyLoginDataIn) service.VerifyLoginDataOut
	UserRefreshToken(ctx context.Context, in service.UserRefreshTokenIn) service.UserRefreshTokenOut
}

type AuthHandler struct {
	tokenService tokenService
	AuthService  AuthService
	responder.Responder
}

func NewAuthHandler(responder responder.Responder, tService tokenService, aService AuthService) *AuthHandler {
	return &AuthHandler{Responder: responder, tokenService: tService, AuthService: aService}
}
