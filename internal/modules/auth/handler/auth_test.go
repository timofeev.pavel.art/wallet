package handler

import (
	"errors"
	"testing"
)

func Test_validatePassword(t *testing.T) {
	tests := []struct {
		name      string
		password  string
		wantErr   bool
		expectErr error
	}{
		{
			name:      "1 digit, 4 upper char, 1 symb, expect false err",
			password:  "AB1!CD",
			wantErr:   false,
			expectErr: nil,
		},
		{
			name:      "len 5 correct password, digit at the end",
			password:  "ABC!1",
			wantErr:   false,
			expectErr: nil,
		},
		{
			name:      "len 5 correct password, symb at the end",
			password:  "ABC5!",
			wantErr:   false,
			expectErr: nil,
		},
		{
			name:      "3 digit 2 upper char, 1 lower char, 1 symb, expect false err",
			password:  "123ABa!",
			wantErr:   false,
			expectErr: nil,
		},
		{
			name:      "1 digit, 2 upper char, 2 symb, expect false err",
			password:  "9AS!,",
			wantErr:   false,
			expectErr: nil,
		},
		{
			name:      "2 repeat digit, 2 upper char, 1 symb, expect true err",
			password:  "11AB!",
			wantErr:   true,
			expectErr: ErrRepeatChar,
		},
		{
			name:      "2 digit, 2 repeat upper char, 1 symb, expect true err",
			password:  "12HH!",
			wantErr:   true,
			expectErr: ErrRepeatChar,
		},
		{
			name:      "2 digit, 2 repeat lower char, 1 symb, expect true err",
			password:  "26aa!",
			wantErr:   true,
			expectErr: ErrRepeatChar,
		},
		{
			name:      "2 digit, 1 lower char, 1 upper char, 2 repeat symb, expect true err",
			password:  "12aA!!",
			wantErr:   true,
			expectErr: ErrRepeatChar,
		},
		{
			name:      "password len <5",
			password:  "12a!",
			wantErr:   true,
			expectErr: ErrShortPassword,
		},
		{
			name:      "2 digit, 2 lower char, 1 upper char, 0 symb, expect true err",
			password:  "12asB",
			wantErr:   true,
			expectErr: ErrHasLetDigSymb,
		},
		{
			name:      "0 digit, 2 lower char, 1 upper char, 2 symb, expect true err",
			password:  "asB?*",
			wantErr:   true,
			expectErr: ErrHasLetDigSymb,
		},
		{
			name:      "3 digit, 0 char, 2 symb, expect true err",
			password:  "126:?",
			wantErr:   true,
			expectErr: ErrHasLetDigSymb,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validatePassword(tt.password); (err != nil) != tt.wantErr || !errors.Is(err, tt.expectErr) {
				t.Errorf("validatePassword() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_validateEmail(t *testing.T) {
	tests := []struct {
		name    string
		email   string
		wantErr bool
	}{
		{
			name:    "correct email",
			email:   "example@mail.com",
			wantErr: false,
		},
		{
			name:    "correct short email",
			email:   "e@e.e",
			wantErr: false,
		},
		{
			name:    "incorrect email, @e.e",
			email:   "@e.e",
			wantErr: true,
		},
		{
			name:    "incorrect email, e@.e",
			email:   "e@.e",
			wantErr: true,
		},
		{
			name:    "incorrect email, e@e.",
			email:   "e@e.",
			wantErr: true,
		},
		{
			name:    "incorrect email, wrong structure example.com",
			email:   "example.com",
			wantErr: true,
		},
		{
			name:    "incorrect email, wrong structure example@mail.",
			email:   "example@mail.",
			wantErr: true,
		},
		{
			name:    "incorrect email, wrong structure example@.com",
			email:   "example@.com",
			wantErr: true,
		},
		{
			name:    "incorrect email, wrong structure @example.com",
			email:   "@example.com",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validateEmail(tt.email); (err != nil) != tt.wantErr {
				t.Errorf("validateEmail() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
