package handler

//go:generate easytags $GOFILE json
type RegisterRequest struct {
	Email          string `json:"email"`
	Password       string `json:"password"`
	RetypePassword string `json:"retype_password"`
	FirstName      string `json:"first_name"`
	LastName       string `json:"last_name"`
	CurrencyID     int    `json:"currency_id"`
}

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginResponse struct {
	RefreshToken string `json:"refresh_token"`
	AccessToken  string `json:"access_token"`
	Email        string `json:"email"`
}

type RefreshResponse struct {
	AccessToken string `json:"access_token"`
}
