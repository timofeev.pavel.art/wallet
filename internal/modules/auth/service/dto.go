package service

type UserRegisterIn struct {
	Email      string
	Password   string
	FirstName  string
	LastName   string
	CurrencyID int
}

type UserRegisterOut struct {
	Err error
}

type VerifyLoginDataIn struct {
	Email    string
	Password string
}

type VerifyLoginDataOut struct {
	RefreshToken string
	AccessToken  string
	Err          error
}

type UserRefreshTokenIn struct {
	UserID string
}

type UserRefreshTokenOut struct {
	AccessToken string
	Err         error
}
