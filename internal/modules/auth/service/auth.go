package service

import (
	"context"
	"errors"
	"fmt"

	"github.com/lib/pq"
	"gitlab.com/timofeev.pavel.art/wallet/internal/models"
	"go.uber.org/zap"

	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/cryptohash"
)

// UserRegister method check user data, generate userID and password hash and save to database
func (s *AuthService) UserRegister(ctx context.Context, in UserRegisterIn) UserRegisterOut {
	// Generate password hash
	passwordHash, err := cryptohash.HashPassword(in.Password)
	if err != nil {
		return UserRegisterOut{Err: err}
	}
	fmt.Println(passwordHash)
	userTokenHash := cryptohash.GenerateRandomTokenHash(128)

	// Save user in storage. If error is unique violation, then return already exists user.
	err = s.userStorage.CreateUser(ctx, models.User{
		Email:      in.Email,
		Password:   passwordHash,
		TokenHash:  userTokenHash,
		FirstName:  in.FirstName,
		LastName:   in.LastName,
		CurrencyID: in.CurrencyID,
	})
	if err != nil {
		var v pq.Error
		switch {
		case errors.As(err, &v) && v.Code == "23505":
			return UserRegisterOut{ErrUserAlreadyExists}
		case errors.As(err, &v) && v.Code == "23503":
			return UserRegisterOut{ErrCurrencyDoesNotExist}
		}

		s.log.Error("create user error", zap.Error(err))
		return UserRegisterOut{ErrUserCreate}
	}

	return UserRegisterOut{}
}

// VerifyLoginData method check user data, compare typed & stored password, generate and return refresh & access token
func (s *AuthService) VerifyLoginData(ctx context.Context, in VerifyLoginDataIn) VerifyLoginDataOut {
	// Get user data by email
	user, err := s.userStorage.GetUserByEmail(ctx, in.Email)
	if err != nil {
		s.log.Error("get user by email error", zap.Error(err))
		return VerifyLoginDataOut{Err: ErrUserEmail}
	}

	// Compare typed password & stored password
	if !cryptohash.CheckPassword(user.Password, in.Password) {
		s.log.Warn("check typed password failed")
		return VerifyLoginDataOut{Err: ErrUserCheckPassword}
	}

	// Generate refresh & access token
	refresh, err := s.tokenService.GenerateRefreshToken(user)
	if err != nil {
		s.log.Error("refresh token generate error", zap.Error(err))
		return VerifyLoginDataOut{Err: err}
	}

	access, err := s.tokenService.GenerateAccessToken(user)
	if err != nil {
		s.log.Error("access token generate error", zap.Error(err))
		return VerifyLoginDataOut{Err: err}
	}

	return VerifyLoginDataOut{RefreshToken: refresh, AccessToken: access}
}

// UserRefreshToken method get user from database by userID and generate & return new access token
func (s *AuthService) UserRefreshToken(ctx context.Context, in UserRefreshTokenIn) UserRefreshTokenOut {
	// Get user by userID
	user, err := s.userStorage.GetUserByID(ctx, in.UserID)
	if err != nil {
		s.log.Error("get user by id error", zap.Error(err))
		return UserRefreshTokenOut{Err: err}
	}

	// Generate access token by user
	access, err := s.tokenService.GenerateAccessToken(user)
	if err != nil {
		s.log.Error("access token generate error", zap.Error(err))
		return UserRefreshTokenOut{Err: err}
	}

	return UserRefreshTokenOut{AccessToken: access}
}
