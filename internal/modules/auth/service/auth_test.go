package service

import (
	"context"
	"errors"
	"os"
	"testing"

	"gitlab.com/timofeev.pavel.art/wallet/internal/models"

	"github.com/lib/pq"
	"github.com/stretchr/testify/mock"

	"golang.org/x/crypto/bcrypt"

	"github.com/stretchr/testify/assert"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/logs"
	"gitlab.com/timofeev.pavel.art/wallet/internal/modules/auth/service/mocks"
)

type AuthServiceSuite struct {
	suite.Suite
	*require.Assertions

	userStorage  *mocks.UserProvider
	tokenService *mocks.TokenService

	authService Auther
}

func TestAuthServiceSuite(t *testing.T) {
	suite.Run(t, new(AuthServiceSuite))
}

func (a *AuthServiceSuite) SetupTest() {
	a.Assertions = require.New(a.T())

	a.userStorage = mocks.NewUserProvider(a.T())
	a.tokenService = mocks.NewTokenService(a.T())

	logger := logs.NewLogger("debug", os.Stdout)
	a.authService = NewAuthService(a.tokenService, a.userStorage, logger)
}

func (a *AuthServiceSuite) Test_UserRegister() {
	tests := []struct {
		name   string
		arg    UserRegisterIn
		expect UserRegisterOut
		on     func(storage *mocks.UserProvider)
	}{
		{
			name: "HappyPath",
			arg: UserRegisterIn{
				Email:      "example@mail.com",
				Password:   "12345",
				FirstName:  "John",
				LastName:   "Doe",
				CurrencyID: 0,
			},
			expect: UserRegisterOut{Err: nil},
			on: func(storage *mocks.UserProvider) {
				storage.On("CreateUser", mock.Anything, mock.AnythingOfType("models.User")).
					Return(nil).Once()
			},
		},
		{
			name: "crypto error",
			arg: UserRegisterIn{
				Email:      "example@mail.com",
				Password:   "asdlfj;lasdjgflsafj;lgsdflkgh;adfkasdhl gajd;sfjlad;sfj;ewqrpoweioptup;sda;pfj asdh tgowaeo;rp;qoweap;rfjlsdhglwqolaertjlaj",
				FirstName:  "John",
				LastName:   "Doe",
				CurrencyID: 0,
			},
			expect: UserRegisterOut{Err: bcrypt.ErrPasswordTooLong},
			on:     nil,
		},
		{
			name: "pq 23505 error, user already exists",
			arg: UserRegisterIn{
				Email:      "example@mail.com",
				Password:   "12345",
				FirstName:  "John",
				LastName:   "Doe",
				CurrencyID: 0,
			},
			expect: UserRegisterOut{Err: ErrUserAlreadyExists},
			on: func(storage *mocks.UserProvider) {
				storage.On("CreateUser", mock.Anything, mock.AnythingOfType("models.User")).
					Return(pq.Error{Code: "23505"}).Once()
			},
		},
		{
			name: "pq 23503 error, currency doesn't exists",
			arg: UserRegisterIn{
				Email:      "example@mail.com",
				Password:   "12345",
				FirstName:  "John",
				LastName:   "Doe",
				CurrencyID: 20,
			},
			expect: UserRegisterOut{Err: ErrCurrencyDoesNotExist},
			on: func(storage *mocks.UserProvider) {
				storage.On("CreateUser", mock.Anything, mock.AnythingOfType("models.User")).
					Return(pq.Error{Code: "23503"}).Once()
			},
		},
		{
			name: "pq other errors, fail on create user",
			arg: UserRegisterIn{
				Email:      "example@mail.com",
				Password:   "12345",
				FirstName:  "John",
				LastName:   "Doe",
				CurrencyID: 20,
			},
			expect: UserRegisterOut{Err: ErrUserCreate},
			on: func(storage *mocks.UserProvider) {
				storage.On("CreateUser", mock.Anything, mock.AnythingOfType("models.User")).
					Return(pq.Error{Code: "23520"}).Once()
			},
		},
	}

	for _, tt := range tests {
		a.Run(tt.name, func() {
			if tt.on != nil {
				tt.on(a.userStorage)
			}

			out := a.authService.UserRegister(context.Background(), tt.arg)

			assert.Equal(a.T(), tt.expect, out)
		})
	}
}

func (a *AuthServiceSuite) Test_VerifyLoginData() {
	tests := []struct {
		name      string
		arg       VerifyLoginDataIn
		expect    VerifyLoginDataOut
		onStorage func(storage *mocks.UserProvider)
		onToken   func(token *mocks.TokenService)
	}{
		{
			name: "HappyPath",
			arg: VerifyLoginDataIn{
				Email:    "example@mail.com",
				Password: "12345",
			},
			expect: VerifyLoginDataOut{
				RefreshToken: "RefreshToken",
				AccessToken:  "AccessToken",
				Err:          nil,
			},
			onStorage: func(storage *mocks.UserProvider) {
				storage.On("GetUserByEmail", mock.Anything, mock.AnythingOfType("string")).
					Return(models.User{Password: "$2a$10$oQ0dXkWOyCgUWInAhWPrmOgG9Gd0OsvmnUKiwAjdesRiOz2fMHzPu"}, nil).Once()
			},
			onToken: func(token *mocks.TokenService) {
				token.On("GenerateRefreshToken", mock.AnythingOfType("models.User")).
					Return("RefreshToken", nil).Once()
				token.On("GenerateAccessToken", mock.AnythingOfType("models.User")).
					Return("AccessToken", nil).Once()
			},
		},
		{
			name: "get user by email error",
			arg: VerifyLoginDataIn{
				Email:    "example@mail.com",
				Password: "12345",
			},
			expect: VerifyLoginDataOut{
				RefreshToken: "",
				AccessToken:  "",
				Err:          ErrUserEmail,
			},
			onStorage: func(storage *mocks.UserProvider) {
				storage.On("GetUserByEmail", mock.Anything, mock.AnythingOfType("string")).
					Return(models.User{}, errors.New("someError")).Once()
			},
			onToken: nil,
		},
		{
			name: "compare typed & stored password error",
			arg: VerifyLoginDataIn{
				Email:    "example@mail.com",
				Password: "12345",
			},
			expect: VerifyLoginDataOut{
				RefreshToken: "",
				AccessToken:  "",
				Err:          ErrUserCheckPassword,
			},
			onStorage: func(storage *mocks.UserProvider) {
				storage.On("GetUserByEmail", mock.Anything, mock.AnythingOfType("string")).
					Return(models.User{Password: "wrongPassword"}, nil).Once()
			},
			onToken: nil,
		},
		{
			name: "generate refresh token error",
			arg: VerifyLoginDataIn{
				Email:    "example@mail.com",
				Password: "12345",
			},
			expect: VerifyLoginDataOut{
				RefreshToken: "",
				AccessToken:  "",
				Err:          errors.New("refresh token error"),
			},
			onStorage: func(storage *mocks.UserProvider) {
				storage.On("GetUserByEmail", mock.Anything, mock.AnythingOfType("string")).
					Return(models.User{Password: "$2a$10$oQ0dXkWOyCgUWInAhWPrmOgG9Gd0OsvmnUKiwAjdesRiOz2fMHzPu"}, nil).Once()
			},
			onToken: func(token *mocks.TokenService) {
				token.On("GenerateRefreshToken", mock.AnythingOfType("models.User")).
					Return("", errors.New("refresh token error")).Once()
			},
		},
		{
			name: "generate access token error",
			arg: VerifyLoginDataIn{
				Email:    "example@mail.com",
				Password: "12345",
			},
			expect: VerifyLoginDataOut{
				RefreshToken: "",
				AccessToken:  "",
				Err:          errors.New("access token error"),
			},
			onStorage: func(storage *mocks.UserProvider) {
				storage.On("GetUserByEmail", mock.Anything, mock.AnythingOfType("string")).
					Return(models.User{Password: "$2a$10$oQ0dXkWOyCgUWInAhWPrmOgG9Gd0OsvmnUKiwAjdesRiOz2fMHzPu"}, nil).Once()
			},
			onToken: func(token *mocks.TokenService) {
				token.On("GenerateRefreshToken", mock.AnythingOfType("models.User")).
					Return("RefreshToken", nil).Once()
				token.On("GenerateAccessToken", mock.AnythingOfType("models.User")).
					Return("", errors.New("access token error")).Once()
			},
		},
	}

	for _, tt := range tests {
		a.Run(tt.name, func() {
			if tt.onStorage != nil {
				tt.onStorage(a.userStorage)
			}
			if tt.onToken != nil {
				tt.onToken(a.tokenService)
			}

			out := a.authService.VerifyLoginData(context.Background(), tt.arg)

			assert.Equal(a.T(), tt.expect, out)
		})
	}
}

func (a *AuthServiceSuite) Test_UserRefreshToken() {
	tests := []struct {
		name      string
		arg       UserRefreshTokenIn
		expect    UserRefreshTokenOut
		onStorage func(storage *mocks.UserProvider)
		onToken   func(token *mocks.TokenService)
	}{
		{
			name: "HappyPath",
			arg: UserRefreshTokenIn{
				UserID: "0",
			},
			expect: UserRefreshTokenOut{
				AccessToken: "AccessToken",
				Err:         nil,
			},
			onStorage: func(storage *mocks.UserProvider) {
				storage.On("GetUserByID", mock.Anything, mock.AnythingOfType("string")).
					Return(models.User{ID: "0", Email: "example@mai.com", Password: "12345"}, nil).Once()
			},
			onToken: func(token *mocks.TokenService) {
				token.On("GenerateAccessToken", mock.AnythingOfType("models.User")).
					Return("AccessToken", nil).Once()
			},
		},
		{
			name: "get user by id error",
			arg: UserRefreshTokenIn{
				UserID: "0",
			},
			expect: UserRefreshTokenOut{
				AccessToken: "",
				Err:         errors.New("repository error"),
			},
			onStorage: func(storage *mocks.UserProvider) {
				storage.On("GetUserByID", mock.Anything, mock.AnythingOfType("string")).
					Return(models.User{}, errors.New("repository error")).Once()
			},
			onToken: nil,
		},
		{
			name: "generate access token error",
			arg: UserRefreshTokenIn{
				UserID: "0",
			},
			expect: UserRefreshTokenOut{
				AccessToken: "",
				Err:         errors.New("access token error"),
			},
			onStorage: func(storage *mocks.UserProvider) {
				storage.On("GetUserByID", mock.Anything, mock.AnythingOfType("string")).
					Return(models.User{ID: "0", Email: "example@mai.com", Password: "12345"}, nil).Once()
			},
			onToken: func(token *mocks.TokenService) {
				token.On("GenerateAccessToken", mock.AnythingOfType("models.User")).
					Return("", errors.New("access token error")).Once()
			},
		},
	}

	for _, tt := range tests {
		a.Run(tt.name, func() {
			if tt.onStorage != nil {
				tt.onStorage(a.userStorage)
			}
			if tt.onToken != nil {
				tt.onToken(a.tokenService)
			}

			out := a.authService.UserRefreshToken(context.Background(), tt.arg)

			assert.Equal(a.T(), tt.expect, out)
		})
	}
}
