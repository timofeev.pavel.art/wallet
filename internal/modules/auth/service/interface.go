package service

import (
	"context"
	"errors"

	"gitlab.com/timofeev.pavel.art/wallet/internal/models"
	"go.uber.org/zap"
)

var (
	ErrUserAlreadyExists    = errors.New("user already exists")
	ErrUserCreate           = errors.New("create user error")
	ErrCurrencyDoesNotExist = errors.New("currency doesn't exists")
	ErrUserEmail            = errors.New("get by email user error")
	ErrUserCheckPassword    = errors.New("check typed password failed")
)

type Auther interface {
	UserRegister(ctx context.Context, in UserRegisterIn) UserRegisterOut
	VerifyLoginData(ctx context.Context, in VerifyLoginDataIn) VerifyLoginDataOut
	UserRefreshToken(ctx context.Context, in UserRefreshTokenIn) UserRefreshTokenOut
}

//go:generate mockery --name TokenService
type TokenService interface {
	GenerateAccessToken(user models.User) (string, error)
	GenerateRefreshToken(user models.User) (string, error)
}

//go:generate mockery --name UserProvider
type UserProvider interface {
	CreateUser(ctx context.Context, user models.User) error
	GetUserByEmail(ctx context.Context, email string) (models.User, error)
	GetUserByID(ctx context.Context, userID string) (models.User, error)
}

type AuthService struct {
	tokenService TokenService
	userStorage  UserProvider
	log          *zap.Logger
}

func NewAuthService(tService TokenService, uStorage UserProvider, log *zap.Logger) *AuthService {
	return &AuthService{
		tokenService: tService,
		userStorage:  uStorage,
		log:          log,
	}
}
