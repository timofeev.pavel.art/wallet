package worker

import (
	"context"
	"io"
	"math"
	"net/http"
	"strings"
	"time"

	"go.uber.org/zap"

	"gitlab.com/timofeev.pavel.art/wallet/internal/config"
	"gitlab.com/timofeev.pavel.art/wallet/internal/models"
)

type ExchangeWorker interface {
	UpsertData(ctx context.Context)
}

type ExchangeProvider interface {
	UpsertExchanges(ctx context.Context, exchange []models.Exchange) error
}

type CurrProvider interface {
	CreateCurrencyList(ctx context.Context, currencies []models.Currency) error
}

type ExchangeWorkerPool struct {
	cfg      config.CurrencyWorker
	eStorage ExchangeProvider
	cStorage CurrProvider
	log      *zap.Logger
}

func NewWorkerPool(cfg config.CurrencyWorker, eStorage ExchangeProvider, cStorage CurrProvider, log *zap.Logger) *ExchangeWorkerPool {
	return &ExchangeWorkerPool{cfg: cfg, eStorage: eStorage, cStorage: cStorage, log: log}
}

func (w *ExchangeWorkerPool) UpsertData(ctx context.Context) {
	err := w.initCurrencyData()
	if err != nil {
		w.log.Fatal("init currency data error", zap.Error(err))
		return
	}

	tick := time.NewTicker(w.cfg.CurrencyTicker * time.Second)
	w.log.Info("getWorker: start work")
	for {
		select {
		case <-ctx.Done():
			w.log.Info("getWorker: stopping worker")
			return
		case <-tick.C:
			rawData, err := w.request()
			if err != nil {
				w.log.Error("worker get request error", zap.Error(err))
			}

			repoData := make([]models.Exchange, 0, len(rawData))
			for key, item := range rawData {
				intRaw, fractRaw := math.Modf(item.Value)

				var (
					intPart   = int(intRaw)
					fractPart = int(fractRaw * 100)
				)

				repoData = append(repoData, models.Exchange{
					Source:   "RUB",
					Target:   key,
					Integer:  intPart,
					Fraction: fractPart,
				})
			}

			if err := w.eStorage.UpsertExchanges(context.Background(), repoData); err != nil {
				w.log.Error("upsert currency error", zap.Error(err))
			}
		}
	}
}

func (w *ExchangeWorkerPool) initCurrencyData() error {
	rawData, err := w.request()
	if err != nil {
		return err
	}

	currList := make([]models.Currency, 0, len(rawData)+1)
	currList = append(currList, models.Currency{Name: "RUB"})
	for key := range rawData {
		currList = append(currList, models.Currency{Name: key})
	}

	return w.cStorage.CreateCurrencyList(context.Background(), currList)
}

func (w *ExchangeWorkerPool) request() (map[string]models.RawCurr, error) {
	payload := strings.NewReader("")
	client := &http.Client{}
	req, err := http.NewRequest("GET", w.cfg.CurrencySource, payload)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json;charset=utf-8")

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
		if err != nil {
			w.log.Error("resp body close error", zap.Error(err))
			return
		}
	}(res.Body)

	data, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	result, err := models.UnmarshalRawCurrency(data)
	if err != nil {
		return nil, err
	}

	return result.Currency, nil
}
