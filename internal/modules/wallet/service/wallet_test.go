package service

import (
	"context"
	"errors"
	"os"
	"testing"

	"gitlab.com/timofeev.pavel.art/wallet/internal/models"

	"gitlab.com/timofeev.pavel.art/wallet/internal/storage"

	"github.com/lib/pq"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/logs"
	"gitlab.com/timofeev.pavel.art/wallet/internal/modules/wallet/service/mocks"
)

type WalletServiceSuite struct {
	suite.Suite
	*require.Assertions

	walletStorage *mocks.WalletProvider

	walletService Walleter
}

func TestWalletServiceSuite(t *testing.T) {
	suite.Run(t, new(WalletServiceSuite))
}

func (w *WalletServiceSuite) SetupTest() {
	w.Assertions = require.New(w.T())

	w.walletStorage = mocks.NewWalletProvider(w.T())

	logger := logs.NewLogger("debug", os.Stdout)
	w.walletService = NewWalletService(w.walletStorage, logger)
}

func (w *WalletServiceSuite) TestWalletService_CreateWallet() {
	tests := []struct {
		name   string
		arg    CreateWalletIn
		expect CreateWalletOut
		on     func(storage *mocks.WalletProvider)
	}{
		{
			name: "HappyPath",
			arg: CreateWalletIn{
				Name:   "example",
				UserID: "1",
			},
			expect: CreateWalletOut{Err: nil},
			on: func(storage *mocks.WalletProvider) {
				storage.On("CreateWallet", mock.Anything, mock.AnythingOfType("models.Wallet")).
					Return(nil).Once()
			},
		},
		{
			name: "Error create wallet",
			arg: CreateWalletIn{
				Name:   "example",
				UserID: "1",
			},
			expect: CreateWalletOut{Err: ErrCreateWallet},
			on: func(s *mocks.WalletProvider) {
				s.On("CreateWallet", mock.Anything, mock.AnythingOfType("models.Wallet")).
					Return(errors.New("some error")).Once()
			},
		},
	}
	for _, tt := range tests {
		w.Run(tt.name, func() {
			if tt.on != nil {
				tt.on(w.walletStorage)
			}

			out := w.walletService.CreateWallet(context.Background(), tt.arg)

			assert.Equal(w.T(), tt.expect, out)
		})
	}
}

func (w *WalletServiceSuite) TestWalletService_DeleteWallet() {
	tests := []struct {
		name   string
		arg    DeleteWalletIn
		expect DeleteWalletOut
		on     func(storage *mocks.WalletProvider)
	}{
		{
			name:   "HappyPath",
			arg:    DeleteWalletIn{WalletID: 1},
			expect: DeleteWalletOut{Err: nil},
			on: func(s *mocks.WalletProvider) {
				s.On("DeleteWalletByID", mock.Anything, mock.AnythingOfType("int")).
					Return(nil).Once()
			},
		},
		{
			name:   "pq error 23503, wallet does not exist",
			arg:    DeleteWalletIn{WalletID: 1},
			expect: DeleteWalletOut{Err: ErrWalletDoesNotExist},
			on: func(s *mocks.WalletProvider) {
				s.On("DeleteWalletByID", mock.Anything, mock.AnythingOfType("int")).
					Return(pq.Error{Code: "23503"}).Once()
			},
		},
		{
			name:   "delete wallet error",
			arg:    DeleteWalletIn{WalletID: 1},
			expect: DeleteWalletOut{Err: ErrDeleteWallet},
			on: func(s *mocks.WalletProvider) {
				s.On("DeleteWalletByID", mock.Anything, mock.AnythingOfType("int")).
					Return(errors.New("some error")).Once()
			},
		},
	}
	for _, tt := range tests {
		w.Run(tt.name, func() {
			if tt.on != nil {
				tt.on(w.walletStorage)
			}

			out := w.walletService.DeleteWallet(context.Background(), tt.arg)

			assert.Equal(w.T(), tt.expect, out)
		})
	}
}

func (w *WalletServiceSuite) TestWalletService_EditWallet() {
	tests := []struct {
		name   string
		arg    EditWalletIn
		expect EditWalletOut
		on     func(storage *mocks.WalletProvider)
	}{
		{
			name:   "HappyPath",
			arg:    EditWalletIn{WalletID: 1, Name: "example1"},
			expect: EditWalletOut{Err: nil},
			on: func(s *mocks.WalletProvider) {
				s.On("UpdateWalletByID", mock.Anything, mock.AnythingOfType("int"), mock.AnythingOfType("models.Wallet")).
					Return(nil).Once()
			},
		},
		{
			name:   "pq error 23503, wallet does not exist",
			arg:    EditWalletIn{WalletID: 1, Name: "example1"},
			expect: EditWalletOut{Err: ErrWalletDoesNotExist},
			on: func(s *mocks.WalletProvider) {
				s.On("UpdateWalletByID", mock.Anything, mock.AnythingOfType("int"), mock.AnythingOfType("models.Wallet")).
					Return(pq.Error{Code: "23503"}).Once()
			},
		},
		{
			name:   "update wallet error",
			arg:    EditWalletIn{WalletID: 1, Name: "example1"},
			expect: EditWalletOut{Err: ErrUpdateWallet},
			on: func(s *mocks.WalletProvider) {
				s.On("UpdateWalletByID", mock.Anything, mock.AnythingOfType("int"), mock.AnythingOfType("models.Wallet")).
					Return(errors.New("some error")).Once()
			},
		},
	}
	for _, tt := range tests {
		w.Run(tt.name, func() {
			if tt.on != nil {
				tt.on(w.walletStorage)
			}

			out := w.walletService.EditWallet(context.Background(), tt.arg)

			assert.Equal(w.T(), tt.expect, out)
		})
	}
}

func (w *WalletServiceSuite) TestWalletService_GetWalletById() {
	tests := []struct {
		name   string
		arg    GetWalletByIdIn
		expect GetWalletByIdOut
		on     func(storage *mocks.WalletProvider)
	}{
		{
			name: "HappyPath",
			arg:  GetWalletByIdIn{WalletID: 1},
			expect: GetWalletByIdOut{Wallet: models.Wallet{
				ID:     1,
				Name:   "example",
				UserID: "abcd1234",
			}, Err: nil},
			on: func(s *mocks.WalletProvider) {
				s.On("GetWalletByID", mock.Anything, mock.AnythingOfType("int")).
					Return(models.Wallet{
						ID:     1,
						Name:   "example",
						UserID: "abcd1234",
					}, nil).Once()
			},
		},
		{
			name:   "wallet does not exist",
			arg:    GetWalletByIdIn{WalletID: 1},
			expect: GetWalletByIdOut{Err: ErrWalletDoesNotExist},
			on: func(s *mocks.WalletProvider) {
				s.On("GetWalletByID", mock.Anything, mock.AnythingOfType("int")).
					Return(models.Wallet{}, storage.ErrWalletDoesNotExist).Once()
			},
		},
		{
			name:   "pq error 23503, wallet does not exist",
			arg:    GetWalletByIdIn{WalletID: 1},
			expect: GetWalletByIdOut{Err: ErrWalletDoesNotExist},
			on: func(s *mocks.WalletProvider) {
				s.On("GetWalletByID", mock.Anything, mock.AnythingOfType("int")).
					Return(models.Wallet{}, pq.Error{Code: "23503"}).Once()
			},
		},
		{
			name:   "get wallet error",
			arg:    GetWalletByIdIn{WalletID: 1},
			expect: GetWalletByIdOut{Err: ErrGetWallet},
			on: func(s *mocks.WalletProvider) {
				s.On("GetWalletByID", mock.Anything, mock.AnythingOfType("int")).
					Return(models.Wallet{}, errors.New("some error")).Once()
			},
		},
	}
	for _, tt := range tests {
		w.Run(tt.name, func() {
			if tt.on != nil {
				tt.on(w.walletStorage)
			}

			out := w.walletService.GetWalletById(context.Background(), tt.arg)

			assert.Equal(w.T(), tt.expect, out)
		})
	}
}

func (w *WalletServiceSuite) TestWalletService_GetWalletList() {
	tests := []struct {
		name   string
		arg    GetWalletListIn
		expect GetWalletListOut
		on     func(storage *mocks.WalletProvider)
	}{
		{
			name: "HappyPath",
			arg:  GetWalletListIn{UserID: "abcd1234"},
			expect: GetWalletListOut{Wallets: []models.Wallet{
				{
					ID:     1,
					Name:   "example1",
					UserID: "abcd1234",
				},
				{
					ID:     2,
					Name:   "example2",
					UserID: "abcd1234",
				}}, Err: nil},
			on: func(s *mocks.WalletProvider) {
				s.On("GetWalletListByUserID", mock.Anything, mock.AnythingOfType("string")).
					Return([]models.Wallet{
						{
							ID:     1,
							Name:   "example1",
							UserID: "abcd1234",
						},
						{
							ID:     2,
							Name:   "example2",
							UserID: "abcd1234",
						}}, nil).Once()
			},
		},
		{
			name:   "get wallet error",
			arg:    GetWalletListIn{UserID: "abcd1234"},
			expect: GetWalletListOut{Err: ErrGetWallet},
			on: func(s *mocks.WalletProvider) {
				s.On("GetWalletListByUserID", mock.Anything, mock.AnythingOfType("string")).
					Return(nil, errors.New("some error")).Once()
			},
		},
	}
	for _, tt := range tests {
		w.Run(tt.name, func() {
			if tt.on != nil {
				tt.on(w.walletStorage)
			}

			out := w.walletService.GetWalletList(context.Background(), tt.arg)

			assert.Equal(w.T(), tt.expect, out)
		})
	}
}
