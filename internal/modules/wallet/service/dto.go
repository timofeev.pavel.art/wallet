package service

import "gitlab.com/timofeev.pavel.art/wallet/internal/models"

type CreateWalletIn struct {
	Name   string
	UserID string
}

type CreateWalletOut struct {
	Err error
}

type EditWalletIn struct {
	WalletID int
	Name     string
}

type EditWalletOut struct {
	Err error
}

type GetWalletByIdIn struct {
	WalletID int
}

type GetWalletByIdOut struct {
	Wallet models.Wallet
	Err    error
}

type GetWalletListIn struct {
	UserID string
}

type GetWalletListOut struct {
	Wallets []models.Wallet
	Err     error
}

type DeleteWalletIn struct {
	WalletID int
}

type DeleteWalletOut struct {
	Err error
}
