package service

import (
	"context"
	"errors"

	"gitlab.com/timofeev.pavel.art/wallet/internal/storage"

	"github.com/lib/pq"
	"go.uber.org/zap"

	"gitlab.com/timofeev.pavel.art/wallet/internal/models"
)

// CreateWallet method create new wallet by user id and name
func (w *WalletService) CreateWallet(ctx context.Context, in CreateWalletIn) CreateWalletOut {
	err := w.walletStorage.CreateWallet(ctx, models.Wallet{
		Name:   in.Name,
		UserID: in.UserID,
	})

	if err != nil {
		w.log.Error("create wallet error", zap.Error(err))
		return CreateWalletOut{Err: ErrCreateWallet}
	}

	return CreateWalletOut{}
}

// EditWallet method update wallet info by id
func (w *WalletService) EditWallet(ctx context.Context, in EditWalletIn) EditWalletOut {
	err := w.walletStorage.UpdateWalletByID(ctx, in.WalletID, models.Wallet{Name: in.Name})
	if err != nil {
		var v pq.Error
		if errors.As(err, &v) && v.Code == "23503" {
			return EditWalletOut{Err: ErrWalletDoesNotExist}
		}

		w.log.Error("update wallet by id error", zap.Error(err))
		return EditWalletOut{Err: ErrUpdateWallet}
	}

	return EditWalletOut{}
}

// GetWalletById method return wallet by id. If wallet does not exist, return error
func (w *WalletService) GetWalletById(ctx context.Context, in GetWalletByIdIn) GetWalletByIdOut {
	wallet, err := w.walletStorage.GetWalletByID(ctx, in.WalletID)
	if err != nil {
		var v pq.Error
		if errors.Is(err, storage.ErrWalletDoesNotExist) || errors.As(err, &v) && v.Code == "23503" {
			return GetWalletByIdOut{Err: ErrWalletDoesNotExist}
		}

		w.log.Error("get wallet by id error", zap.Error(err))
		return GetWalletByIdOut{Err: ErrGetWallet}
	}

	return GetWalletByIdOut{Wallet: wallet}
}

// GetWalletList method get all wallets from repository by user id
func (w *WalletService) GetWalletList(ctx context.Context, in GetWalletListIn) GetWalletListOut {
	wallets, err := w.walletStorage.GetWalletListByUserID(ctx, in.UserID)
	if err != nil {
		w.log.Error("get wallet list error", zap.Error(err))
		return GetWalletListOut{Err: ErrGetWallet}
	}

	return GetWalletListOut{Wallets: wallets}
}

// DeleteWallet method delete wallet by wallet id
func (w *WalletService) DeleteWallet(ctx context.Context, in DeleteWalletIn) DeleteWalletOut {
	err := w.walletStorage.DeleteWalletByID(ctx, in.WalletID)

	if err != nil {
		var v pq.Error
		if errors.As(err, &v) && v.Code == "23503" {
			return DeleteWalletOut{Err: ErrWalletDoesNotExist}
		}

		w.log.Error("delete wallet error", zap.Error(err))
		return DeleteWalletOut{Err: ErrDeleteWallet}
	}

	return DeleteWalletOut{}
}
