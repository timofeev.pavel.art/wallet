package service

import (
	"context"
	"errors"

	"gitlab.com/timofeev.pavel.art/wallet/internal/models"
	"go.uber.org/zap"
)

var (
	ErrCreateWallet       = errors.New("create wallet error")
	ErrDeleteWallet       = errors.New("delete wallet error")
	ErrWalletDoesNotExist = errors.New("wallet does not exist")
	ErrGetWallet          = errors.New("get wallet error")
	ErrUpdateWallet       = errors.New("update wallet error")
)

type Walleter interface {
	CreateWallet(ctx context.Context, in CreateWalletIn) CreateWalletOut
	EditWallet(ctx context.Context, in EditWalletIn) EditWalletOut
	GetWalletById(ctx context.Context, in GetWalletByIdIn) GetWalletByIdOut
	GetWalletList(ctx context.Context, in GetWalletListIn) GetWalletListOut
	DeleteWallet(ctx context.Context, in DeleteWalletIn) DeleteWalletOut
}

//go:generate mockery --name WalletProvider
type WalletProvider interface {
	CreateWallet(ctx context.Context, wallet models.Wallet) error
	UpdateWalletByID(ctx context.Context, id int, wallet models.Wallet) error
	GetWalletByID(ctx context.Context, walletID int) (models.Wallet, error)
	GetWalletListByUserID(ctx context.Context, userID string) ([]models.Wallet, error)
	DeleteWalletByID(ctx context.Context, walletID int) error
}

type WalletService struct {
	walletStorage WalletProvider
	log           *zap.Logger
}

func NewWalletService(wStorage WalletProvider, log *zap.Logger) *WalletService {
	return &WalletService{
		walletStorage: wStorage,
		log:           log,
	}
}
