package handler

import (
	"context"
	"net/http"
	"strconv"

	"gitlab.com/timofeev.pavel.art/wallet/internal/modules/wallet/service"

	"github.com/go-chi/chi/v5"
)

func (wh *WalletHandler) WalletCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var out service.GetWalletByIdOut

		if walletID := chi.URLParam(r, "walletId"); walletID != "" {
			id, _ := strconv.Atoi(walletID)
			out = wh.walletService.GetWalletById(r.Context(), service.GetWalletByIdIn{WalletID: id})
		} else {
			wh.ErrorNotFound(w, ErrWalletNotFound)
			return
		}

		if out.Err != nil {
			wh.ErrorBadRequest(w, out.Err)
			return
		}

		ctx := context.WithValue(r.Context(), UserWalletRequest{}, out.Wallet)
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}
