package handler

import "gitlab.com/timofeev.pavel.art/wallet/internal/models"

//go:generate easytags $GOFILE json
type CreateWalletRequest struct {
	Name string `json:"name"`
}

type EditWalletRequest struct {
	Name string `json:"name"`
}

type GetWalletListResponse struct {
	Wallets []models.Wallet `json:"wallets"`
}

type DeleteWalletRequest struct {
	ID int `json:"id"`
}

type UserWalletRequest struct {
}
