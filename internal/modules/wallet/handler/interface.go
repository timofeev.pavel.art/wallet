package handler

import (
	"context"
	"errors"
	"net/http"

	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/responder"
	"gitlab.com/timofeev.pavel.art/wallet/internal/modules/wallet/service"
)

var (
	ErrInvalidData     = errors.New("invalid input data")
	ErrEmptyWalletName = errors.New("wallet name cannot be empty")
	ErrWalletNotFound  = errors.New("wallet does not exist")
	ErrParseCtx        = errors.New("error when parse wallet from context")
)

type Walleter interface {
	Create(w http.ResponseWriter, r *http.Request)
	Edit(w http.ResponseWriter, r *http.Request)
	GetList(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)

	WalletCtx(next http.Handler) http.Handler
}

type WalletService interface {
	CreateWallet(ctx context.Context, in service.CreateWalletIn) service.CreateWalletOut
	EditWallet(ctx context.Context, in service.EditWalletIn) service.EditWalletOut
	GetWalletById(ctx context.Context, in service.GetWalletByIdIn) service.GetWalletByIdOut
	GetWalletList(ctx context.Context, in service.GetWalletListIn) service.GetWalletListOut
	DeleteWallet(ctx context.Context, in service.DeleteWalletIn) service.DeleteWalletOut
}

type tokenService interface {
	ExtractUserFromAccessToken(r *http.Request) (string, error)
}

type WalletHandler struct {
	walletService WalletService
	tokenService  tokenService
	responder.Responder
}

func NewWalletHandler(responder responder.Responder, wService WalletService, tService tokenService) *WalletHandler {
	return &WalletHandler{
		walletService: wService,
		Responder:     responder,
		tokenService:  tService,
	}
}
