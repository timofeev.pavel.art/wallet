package handler

import (
	"errors"
	"testing"
)

func Test_checkWalletName(t *testing.T) {
	tests := []struct {
		name    string
		arg     string
		wantErr bool
		err     error
	}{
		{
			name:    "HappyPath 1",
			arg:     "example",
			wantErr: false,
			err:     nil,
		},
		{
			name:    "HappyPath 2",
			arg:     "exam ple",
			wantErr: false,
			err:     nil,
		},
		{
			name:    "HappyPath 3",
			arg:     "e x a m p l e",
			wantErr: false,
			err:     nil,
		},
		{
			name:    "Empty string",
			arg:     "",
			wantErr: true,
			err:     ErrEmptyWalletName,
		},
		{
			name:    "One space string",
			arg:     " ",
			wantErr: true,
			err:     ErrEmptyWalletName,
		},
		{
			name:    "Many space string",
			arg:     "     ",
			wantErr: true,
			err:     ErrEmptyWalletName,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := checkWalletName(tt.arg); (err != nil) != (tt.wantErr && errors.Is(err, tt.err)) {
				t.Errorf("checkWalletName() error = %v, wantErr %v", err, tt.err)
			}
		})
	}
}
