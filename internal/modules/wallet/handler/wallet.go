package handler

import (
	"encoding/json"
	"net/http"
	"strings"

	"gitlab.com/timofeev.pavel.art/wallet/internal/models"

	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/responder"
	"gitlab.com/timofeev.pavel.art/wallet/internal/modules/wallet/service"
)

// Create godoc
// @Summary Create new wallet
// @Description Create new wallet from request data, using user id from refresh token
// @Tags wallet
// @Accept json
// @Produce json
// @Param data body CreateWalletRequest true "Wallet data"
// @Success 200 {object} responder.Response
// @Failure 400 {object} responder.Response
// @Router /wallet/create [post]
// @Security Bearer
func (wh *WalletHandler) Create(w http.ResponseWriter, r *http.Request) {
	userID, err := wh.tokenService.ExtractUserFromAccessToken(r)
	if err != nil {
		wh.ErrorBadRequest(w, err)
		return
	}

	var req CreateWalletRequest

	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		wh.ErrorBadRequest(w, ErrInvalidData)
		return
	}

	if err = checkWalletName(req.Name); err != nil {
		wh.ErrorBadRequest(w, err)
		return
	}

	out := wh.walletService.CreateWallet(r.Context(), service.CreateWalletIn{
		Name:   req.Name,
		UserID: userID,
	})

	if out.Err != nil {
		wh.ErrorBadRequest(w, out.Err)
		return
	}

	wh.OutputJSON(w, responder.Response{
		Success: true,
		Message: "Wallet created successfully!",
	})
}

func checkWalletName(name string) error {
	strip := strings.ReplaceAll(name, " ", "")

	if len(strip) == 0 {
		return ErrEmptyWalletName
	}

	return nil
}

// Edit godoc
// @Summary Edit wallet
// @Description Edit wallet by id
// @Tags wallet
// @Accept json
// @Produce json
// @Param data body EditWalletRequest true "Wallet data"
// @Success 200 {object} responder.Response
// @Failure 400 {object} responder.Response
// @Router /wallet/{walletId}/edit [patch]
// @Security Bearer
func (wh *WalletHandler) Edit(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	wallet, ok := ctx.Value(UserWalletRequest{}).(models.Wallet)
	if !ok {
		wh.ErrorInternal(w, ErrParseCtx)
		return
	}

	var req EditWalletRequest

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		wh.ErrorBadRequest(w, ErrInvalidData)
		return
	}

	if err := checkWalletName(req.Name); err != nil {
		wh.ErrorBadRequest(w, err)
		return
	}

	out := wh.walletService.EditWallet(r.Context(), service.EditWalletIn{
		WalletID: wallet.ID,
		Name:     req.Name,
	})

	if out.Err != nil {
		wh.ErrorBadRequest(w, out.Err)
		return
	}

	wh.OutputJSON(w, responder.Response{
		Success: true,
		Message: "Wallet updated successfully!",
	})
}

// GetList godoc
// @Summary GetList wallet
// @Description GetList wallet by id
// @Tags wallet
// @Produce json
// @Success 200 {object} responder.Response
// @Failure 400 {object} responder.Response
// @Router /wallets [get]
// @Security Bearer
func (wh *WalletHandler) GetList(w http.ResponseWriter, r *http.Request) {
	userID, err := wh.tokenService.ExtractUserFromAccessToken(r)
	if err != nil {
		wh.ErrorBadRequest(w, err)
		return
	}

	out := wh.walletService.GetWalletList(r.Context(), service.GetWalletListIn{UserID: userID})

	if out.Err != nil {
		wh.ErrorBadRequest(w, out.Err)
		return
	}

	wh.OutputJSON(w, responder.Response{
		Success: true,
		Message: "Get all wallet by id",
		Data:    GetWalletListResponse{Wallets: out.Wallets},
	})
}

// Delete godoc
// @Summary Delete wallet
// @Description Delete wallet by id
// @Tags wallet
// @Accept json
// @Produce json
// @Param data body DeleteWalletRequest true "Wallet data"
// @Success 200 {object} responder.Response
// @Failure 400 {object} responder.Response
// @Router /wallet/{walletId}/delete [delete]
// @Security Bearer
func (wh *WalletHandler) Delete(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	wallet, ok := ctx.Value(UserWalletRequest{}).(models.Wallet)
	if !ok {
		wh.ErrorInternal(w, ErrParseCtx)
		return
	}

	out := wh.walletService.DeleteWallet(r.Context(), service.DeleteWalletIn{
		WalletID: wallet.ID,
	})

	if out.Err != nil {
		wh.ErrorBadRequest(w, out.Err)
		return
	}

	wh.OutputJSON(w, responder.Response{
		Success: true,
		Message: "Wallet deleted successfully!",
	})
}
