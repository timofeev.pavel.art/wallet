package modules

import (
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/token"
	authService "gitlab.com/timofeev.pavel.art/wallet/internal/modules/auth/service"
	categoryService "gitlab.com/timofeev.pavel.art/wallet/internal/modules/category/service"
	walletService "gitlab.com/timofeev.pavel.art/wallet/internal/modules/wallet/service"
	"gitlab.com/timofeev.pavel.art/wallet/internal/storage"
	"go.uber.org/zap"
)

type Services struct {
	AuthService     authService.Auther
	WalletService   walletService.Walleter
	CategoryService categoryService.Categorier
}

func NewServices(storage storage.Storage, tokenMgr token.TokenManager, log *zap.Logger) *Services {
	return &Services{
		AuthService:     authService.NewAuthService(tokenMgr, storage, log),
		WalletService:   walletService.NewWalletService(storage, log),
		CategoryService: categoryService.NewCategoryService(storage, log),
	}
}
