package models

import (
	"encoding/json"
)

//go:generate easytags $GOFILE json,bd

type Currency struct {
	ID   int    `json:"id" db:"id_currency"`
	Name string `json:"name" db:"name"`
}

type RawCurrList struct {
	Currency map[string]RawCurr `json:"Valute" db:"currency"`
}

type RawCurr struct {
	Value float64 `json:"Value" db:"value"`
}

func UnmarshalRawCurrency(data []byte) (RawCurrList, error) {
	var r RawCurrList
	err := json.Unmarshal(data, &r)
	return r, err
}
