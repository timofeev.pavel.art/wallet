package models

//go:generate easytags $GOFILE json,db
type Wallet struct {
	ID     int    `json:"id" db:"id_wallet"`
	Name   string `json:"name" db:"name"`
	UserID string `json:"user_id" db:"id_user"`
}
