package models

import (
	"time"
)

//go:generate easytags $GOFILE json,db

type Exchange struct {
	ID        int       `json:"id" db:"id_exchange"`
	Source    string    `json:"source" db:"source"`
	Target    string    `json:"target" db:"target"`
	Integer   int       `json:"integer" db:"integer"`
	Fraction  int       `json:"fraction" db:"fraction"`
	UpdatedAt time.Time `json:"update_at" db:"updated_at"`
}
