package models

//go:generate easytags $GOFILE json,db
type Category struct {
	ID   int    `json:"id" db:"id_category"`
	Name string `json:"name" db:"name"`
}
