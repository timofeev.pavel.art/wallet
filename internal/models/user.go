package models

import "time"

//go:generate easytags $GOFILE json,db
type User struct {
	ID         string    `json:"id" db:"id_user"`
	Email      string    `json:"email" db:"email"`
	Password   string    `json:"password" db:"password"`
	TokenHash  string    `json:"token_hash" db:"token_hash"`
	FirstName  string    `json:"first_name" db:"first_name"`
	LastName   string    `json:"last_name" db:"last_name"`
	CurrencyID int       `json:"currency_id" db:"id_currency"`
	CreatedAt  time.Time `json:"created_at" db:"created_at"`
	UpdatedAt  time.Time `json:"updated_at" db:"updated_at"`
}
