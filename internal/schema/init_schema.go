package schema

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
)

var (
	categoryTable = `CREATE TABLE IF NOT EXISTS category (
		id_category SERIAL PRIMARY KEY,
		name CHAR(50) UNIQUE 
	)`
	walletsTable = `CREATE TABLE IF NOT EXISTS wallet (
		id_wallet SERIAL PRIMARY KEY,
		name CHAR(200),
		id_user INTEGER,
		FOREIGN KEY(id_user) REFERENCES users (id_user) ON DELETE CASCADE
	)`
	currencyTable = `CREATE TABLE IF NOT EXISTS currency (
		id_currency SERIAL PRIMARY KEY,
		name CHAR(10) UNIQUE 
	)`
	exchangeTable = `CREATE TABLE IF NOT EXISTS exchange (
		id_exchange SERIAL PRIMARY KEY,
		id_source INTEGER NOT NULL ,
		id_target INTEGER NOT NULL ,
		integer INTEGER,
		fraction INTEGER,
		updated_at TIMESTAMP,
		FOREIGN KEY(id_source) REFERENCES currency(id_currency),
		FOREIGN KEY(id_target) REFERENCES currency(id_currency),
		UNIQUE (id_source, id_target)
	)`
	recordsTable = `CREATE TABLE IF NOT EXISTS record (
		id_record SERIAL PRIMARY KEY,
		description CHAR(1024),
		created_at TIMESTAMP,
		id_wallet INTEGER,
		id_category INTEGER,
		id_currency INTEGER,
		is_transfer BOOLEAN DEFAULT FALSE,
		FOREIGN KEY(id_wallet) REFERENCES wallet(id_wallet) ON DELETE CASCADE,
		FOREIGN KEY(id_category) REFERENCES category(id_category) ON DELETE SET NULL,
		FOREIGN KEY(id_currency) REFERENCES currency(id_currency) ON DELETE SET NULL
	)`
	recordsTransferTable = `CREATE TABLE IF NOT EXISTS record_transfer (
		id_record_transfer SERIAL PRIMARY KEY,
		id_record INTEGER,
		id_wallet_from INTEGER,
		id_wallet_to INTEGER,
		FOREIGN KEY(id_wallet_from) REFERENCES wallet(id_wallet) ON DELETE SET NULL,
		FOREIGN KEY(id_wallet_from) REFERENCES wallet(id_wallet) ON DELETE SET NULL,
		FOREIGN KEY(id_record) REFERENCES record(id_record) ON DELETE CASCADE
	)`
	userTable = `CREATE TABLE IF NOT EXISTS users (
		id_user SERIAL PRIMARY KEY,
		first_name CHAR(100),
		last_name CHAR(100),
		email CHAR(255) UNIQUE,
		password CHAR(1024),
		token_hash CHAR(128),
		created_at TIMESTAMP,
		updated_at TIMESTAMP,
		id_currency INTEGER,
		FOREIGN KEY(id_currency) REFERENCES currency(id_currency)
	)`
)

func InitScheme(db *sqlx.DB) error {
	tx, err := db.BeginTx(context.Background(), &sql.TxOptions{})
	if err != nil {
		_ = tx.Rollback()
		return err
	}
	tables := []string{
		currencyTable,
		exchangeTable,
		userTable,
		categoryTable,
		walletsTable,
		recordsTable,
		recordsTransferTable,
	}

	for _, table := range tables {
		_, err = tx.Exec(table)
		if err != nil {
			_ = tx.Rollback()
			return err
		}
	}

	return tx.Commit()
}
