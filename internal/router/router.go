package router

import (
	httpSwagger "github.com/swaggo/http-swagger"
	"gitlab.com/timofeev.pavel.art/wallet/docs"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/token"
	"gitlab.com/timofeev.pavel.art/wallet/internal/modules"
)

// @title           Swagger Example API
// @version         1.0
// @description     This is a sample server celler server.
// @termsOfService  http://swagger.io/terms/

// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html

// @host      localhost:8080
// @BasePath  /api/v1

// @securityDefinitions.apikey Bearer
// @in header
// @name Authorization

// @externalDocs.description  OpenAPI
// @externalDocs.url          https://swagger.io/resources/open-api/

func NewRouter(handlers *modules.Controllers, tokenManager token.TokenManager) http.Handler {
	r := chi.NewRouter()
	setSwaggerRoutes(r)

	r.Mount("/api/v1", NewRouterV1(handlers, tokenManager))
	return r
}

func setSwaggerRoutes(r *chi.Mux) {
	docs.SwaggerInfo.Title = "Swagger Wallet Api"
	docs.SwaggerInfo.Description = "This is a sample server Wallet server."
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	r.Get("/swagger/*", httpSwagger.WrapHandler)
}
