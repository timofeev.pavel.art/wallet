package router

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/token"
	"gitlab.com/timofeev.pavel.art/wallet/internal/modules"
)

func NewRouterV1(handlers *modules.Controllers, token token.TokenManager) *chi.Mux {
	r := chi.NewRouter()

	authHandler := handlers.AuthHandler

	r.Post("/register", authHandler.Register)
	r.Post("/login", authHandler.Login)

	r.Route("/refresh_token", func(r chi.Router) {
		r.Use(token.MiddlewareValidateRefreshToken)
		r.Get("/", authHandler.RefreshToken)
	})

	walletHandler := handlers.WalletHandler
	r.Route("/wallets", func(r chi.Router) {
		r.Use(token.MiddlewareValidateAccessToken)
		r.Get("/", walletHandler.GetList)
	})
	r.Route("/wallet", func(r chi.Router) {
		r.Use(token.MiddlewareValidateAccessToken)
		r.Post("/create", walletHandler.Create)
		r.Route("/{walletId}", func(r chi.Router) {
			r.Use(walletHandler.WalletCtx)
			r.Patch("/edit", walletHandler.Edit)
			r.Delete("/delete", walletHandler.Delete)
		})
	})

	categoryHandler := handlers.CategoryHandler
	r.Get("/categories", categoryHandler.GetList)
	r.Route("/category", func(r chi.Router) {
		r.Use(token.MiddlewareValidateAccessToken)
		r.Post("/create", categoryHandler.Create)
		r.Route("/{categoryId}", func(r chi.Router) {
			r.Use(categoryHandler.CategoryCtx)
			r.Patch("/edit", categoryHandler.Edit)
			r.Delete("/delete", categoryHandler.Delete)
		})
	})

	return r
}
