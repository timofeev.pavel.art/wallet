package token

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/cryptohash"
	"gitlab.com/timofeev.pavel.art/wallet/internal/models"
	"go.uber.org/zap"
)

// GenerateRefreshToken method generate new refresh token, based on  useID and user tokenHash.
// Token signed by private key, which is stored on the server
func (t *TokenService) GenerateRefreshToken(user models.User) (string, error) {
	claims := RefreshTokenClaims{
		UserID:    user.ID,
		TokenType: RefreshToken,
		CustomKey: cryptohash.GenerateJWTPayloadKey(user.ID, user.TokenHash),
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Minute * t.config.RefreshTTL)),
		},
	}

	signBytes, err := os.ReadFile(t.config.RefreshPrivateKeyPath)
	if err != nil {
		t.logger.Error("unable to read private key", zap.Error(err))
		return "", &ErrTokenGenerate{TokenType: RefreshToken}
	}

	signKey, err := jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		return "", &ErrTokenGenerate{TokenType: RefreshToken}
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	return token.SignedString(signKey)

}

// ValidateRefreshToken parse input refresh token and return userID and custom key from JWT.
// Token parse with public key, which is stored on the server
func (t *TokenService) ValidateRefreshToken(tokenString string) (string, string, error) {
	token, err := jwt.ParseWithClaims(tokenString, &RefreshTokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			t.logger.Error("Unexpected signing method in token")
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		verifyBytes, err := os.ReadFile(t.config.RefreshPublicKeyPath)
		if err != nil {
			t.logger.Error("Unable to parse public key", zap.Error(err))
			return nil, err
		}

		verifyKey, err := jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
		if err != nil {
			t.logger.Error("Unable to parse public key", zap.Error(err))
			return nil, err
		}

		return verifyKey, nil
	})

	if err != nil {
		t.logger.Error("unable to parse claims", zap.Error(err))
		return "", "", err
	}

	claims, ok := token.Claims.(*RefreshTokenClaims)
	if !ok || !token.Valid || claims.UserID == "" || claims.TokenType != RefreshToken {
		return "", "", fmt.Errorf("invalid token: authentication failed")
	}

	return claims.UserID, claims.CustomKey, nil
}

func (t *TokenService) MiddlewareValidateRefreshToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t.logger.Debug("validating refresh token")
		t.logger.Debug(fmt.Sprintf("auth header, %s", r.Header.Get("Authorization")))

		token, err := extractToken(r)
		if err != nil {
			t.ErrorBadRequest(w, err)
			return
		}

		t.logger.Debug(fmt.Sprintf("get token from header %s", token))

		userID, customKey, err := t.ValidateRefreshToken(token)
		if err != nil {
			t.ErrorBadRequest(w, err)
			return
		}

		t.logger.Debug("refresh token validated")

		user, err := t.UserStorage.GetUserByID(r.Context(), userID)
		if err != nil {
			t.ErrorBadRequest(w, ErrUserID)
			return
		}

		actualCustomKey := cryptohash.GenerateJWTPayloadKey(user.ID, user.TokenHash)
		if customKey != actualCustomKey {
			t.ErrorBadRequest(w, ErrInvalidToken)
			return
		}

		ctx := context.WithValue(r.Context(), UserRefreshRequest{}, user)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}

// GenerateAccessToken method generate new access token, based on userID.
// Token signed by private key, which is stored on the server
func (t *TokenService) GenerateAccessToken(user models.User) (string, error) {
	claims := AccessTokenClaims{
		UserID:    user.ID,
		TokenType: AccessToken,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Minute * t.config.RefreshTTL)),
		},
	}

	signBytes, err := os.ReadFile(t.config.RefreshPrivateKeyPath)
	if err != nil {
		t.logger.Error("unable to read private key", zap.Error(err))
		return "", &ErrTokenGenerate{TokenType: AccessToken}
	}

	signKey, err := jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		return "", &ErrTokenGenerate{TokenType: AccessToken}
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	return token.SignedString(signKey)

}

// ValidateAccessToken parse input access token and return userID from JWT.
// Token parse with public key, which is stored on the server
func (t *TokenService) ValidateAccessToken(tokenString string) (string, error) {
	token, err := jwt.ParseWithClaims(tokenString, &AccessTokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			t.logger.Error("Unexpected signing method in token")
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		verifyBytes, err := os.ReadFile(t.config.RefreshPublicKeyPath)
		if err != nil {
			t.logger.Error("Unable to parse public key", zap.Error(err))
			return nil, err
		}

		verifyKey, err := jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
		if err != nil {
			t.logger.Error("Unable to parse public key", zap.Error(err))
			return nil, err
		}

		return verifyKey, nil
	})

	if err != nil {
		t.logger.Error("unable to parse claims", zap.Error(err))
		return "", err
	}

	claims, ok := token.Claims.(*AccessTokenClaims)
	if !ok || !token.Valid || claims.UserID == "" || claims.TokenType != AccessToken {
		return "", fmt.Errorf("invalid token: authentication failed")
	}

	return claims.UserID, nil
}

func (t *TokenService) MiddlewareValidateAccessToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t.logger.Debug("validating access token")

		token, err := extractToken(r)
		if err != nil {
			t.ErrorBadRequest(w, err)
			return
		}

		t.logger.Debug(fmt.Sprintf("get token from header %s", token))

		userID, err := t.ValidateAccessToken(token)
		if err != nil {
			t.ErrorBadRequest(w, err)
			return
		}

		t.logger.Debug("access token validated")

		ctx := context.WithValue(r.Context(), UserAccessRequest{}, userID)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}

// ExtractUserFromRefreshToken method extract from user request refresh token claims and return userID
func (t *TokenService) ExtractUserFromRefreshToken(r *http.Request) (string, error) {
	ctx := r.Context()
	u, ok := ctx.Value(UserRefreshRequest{}).(models.User)
	if !ok {
		return "", ErrUserExtract
	}

	return u.ID, nil
}

// ExtractUserFromAccessToken method extract from user request access token claims and return userID
func (t *TokenService) ExtractUserFromAccessToken(r *http.Request) (string, error) {
	ctx := r.Context()
	u, ok := ctx.Value(UserAccessRequest{}).(string)
	if !ok {
		return "", ErrUserExtract
	}

	return u, nil
}

func extractToken(r *http.Request) (string, error) {
	authHeader := r.Header.Get("Authorization")
	authHeaderContent := strings.Split(authHeader, " ")
	if len(authHeaderContent) != 2 {
		return "", ErrExtractToken
	}

	return authHeaderContent[1], nil
}
