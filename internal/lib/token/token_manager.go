package token

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/timofeev.pavel.art/wallet/internal/config"
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/responder"
	"gitlab.com/timofeev.pavel.art/wallet/internal/models"
	"go.uber.org/zap"
)

type ErrTokenGenerate struct {
	TokenType string
}

func (e *ErrTokenGenerate) Error() string {
	return fmt.Sprintf("Could not generate %s token. Please try later", e.TokenType)
}

var (
	ErrExtractToken = fmt.Errorf("empty or invalid token")
	ErrUserExtract  = fmt.Errorf("type assertion to TokenClaim err")
	ErrInvalidToken = fmt.Errorf("authentication failed. Invalid token")
	ErrUserID       = errors.New("invalid token: wrong userID")
)

const (
	AccessToken  = "access"
	RefreshToken = "refresh"
)

type UserRefreshRequest struct{}
type UserAccessRequest struct{}

type TokenManager interface {
	GenerateAccessToken(user models.User) (string, error)
	ValidateAccessToken(token string) (string, error)
	MiddlewareValidateAccessToken(next http.Handler) http.Handler

	GenerateRefreshToken(user models.User) (string, error)
	ValidateRefreshToken(token string) (string, string, error)
	MiddlewareValidateRefreshToken(next http.Handler) http.Handler

	ExtractUserFromRefreshToken(r *http.Request) (string, error)
	ExtractUserFromAccessToken(r *http.Request) (string, error)
}

type UserProvider interface {
	GetUserByID(ctx context.Context, userID string) (models.User, error)
}

type RefreshTokenClaims struct {
	UserID    string
	CustomKey string
	TokenType string
	jwt.RegisteredClaims
}

type AccessTokenClaims struct {
	UserID    string
	TokenType string
	jwt.RegisteredClaims
}

type TokenService struct {
	logger *zap.Logger
	config config.Token
	responder.Responder
	UserStorage UserProvider
}

func NewTokenManager(l *zap.Logger, cfg config.Token, responder responder.Responder, storage UserProvider) TokenManager {
	return &TokenService{
		logger:      l,
		config:      cfg,
		Responder:   responder,
		UserStorage: storage,
	}
}
