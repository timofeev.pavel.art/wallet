package errs

const (
	NoError = iota
	InternalError
	GeneralError
)
