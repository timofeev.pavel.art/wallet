package cryptohash

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
)

func GenerateJWTPayloadKey(userID, tokenHash string) string {
	h := hmac.New(sha256.New, []byte(tokenHash))
	h.Write([]byte(userID))
	sha := hex.EncodeToString(h.Sum(nil))
	return sha
}
