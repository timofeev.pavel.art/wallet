package run

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"gitlab.com/timofeev.pavel.art/wallet/internal/modules/exchange/worker"

	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/errs"
	"gitlab.com/timofeev.pavel.art/wallet/internal/storage/pgsql"

	"gitlab.com/timofeev.pavel.art/wallet/internal/config"
	"gitlab.com/timofeev.pavel.art/wallet/internal/storage"
	"go.uber.org/zap"
)

type Job struct {
	conf       config.JobConf
	logger     *zap.Logger
	Sig        chan os.Signal
	Storages   storage.Storage
	CurrWorker worker.ExchangeWorker
}

func NewJob(conf config.JobConf, logger *zap.Logger) *Job {
	return &Job{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

func (j *Job) Run() int {
	ctx, cancel := context.WithCancel(context.Background())

	var wg sync.WaitGroup

	wg.Add(2)
	go func() {
		defer wg.Done()
		signal.Notify(j.Sig, syscall.SIGINT, syscall.SIGTERM)
		sigInt := <-j.Sig
		fmt.Println("stop workers", sigInt)
		cancel()
	}()

	go func() {
		defer wg.Done()
		j.CurrWorker.UpsertData(ctx)
	}()

	wg.Wait()

	return errs.NoError
}

func (j *Job) Bootstrap() Runner {
	j.initStorages()
	j.CurrWorker = worker.NewWorkerPool(j.conf.CurrencyWorker, j.Storages, j.Storages, j.logger)

	return j
}

func (j *Job) initStorages() {
	sqlClient, err := storage.NewSQL(j.conf.DB, j.logger)
	if err != nil {
		j.logger.Fatal("error init db", zap.Error(err))
	}

	j.Storages = pgsql.NewPostgresStorage(sqlClient, j.logger)
}
