package run

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"

	"gitlab.com/timofeev.pavel.art/wallet/internal/config"
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/errs"
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/responder"
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/token"
	"gitlab.com/timofeev.pavel.art/wallet/internal/modules"
	"gitlab.com/timofeev.pavel.art/wallet/internal/router"
	"gitlab.com/timofeev.pavel.art/wallet/internal/schema"
	"gitlab.com/timofeev.pavel.art/wallet/internal/server"
	"gitlab.com/timofeev.pavel.art/wallet/internal/storage"
	db "gitlab.com/timofeev.pavel.art/wallet/internal/storage"
	"gitlab.com/timofeev.pavel.art/wallet/internal/storage/pgsql"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

type Application interface {
	Runner
	Bootstraper
}

type Runner interface {
	Run() int
}

type Bootstraper interface {
	Bootstrap() Runner
}

type App struct {
	conf            config.AppConf
	logger          *zap.Logger
	srv             server.Server
	Sig             chan os.Signal
	ResponseManager responder.Responder
	TokenManager    token.TokenManager
	Storages        storage.Storage
	Services        *modules.Services
	Controllers     *modules.Controllers
}

func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

func (a *App) Run() int {
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			a.logger.Error("app: http server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errs.GeneralError
	}

	return errs.NoError
}

func (a *App) Bootstrap() Runner {
	a.appInit()

	r := router.NewRouter(a.Controllers, a.TokenManager)

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}

	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	return a
}

func (a *App) appInit() {
	a.ResponseManager = responder.NewResponder(a.logger)
	a.initStorages()
	a.TokenManager = token.NewTokenManager(a.logger, a.conf.Token, a.ResponseManager, a.Storages)
	a.Services = modules.NewServices(a.Storages, a.TokenManager, a.logger)
	a.Controllers = modules.NewControllers(a.Services, a.ResponseManager, a.TokenManager)
}

func (a *App) initStorages() {
	sqlDb, err := db.NewSQL(a.conf.DB, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}

	if err = schema.InitScheme(sqlDb); err != nil {
		a.logger.Fatal("error init schema", zap.Error(err))
	}

	a.Storages = pgsql.NewPostgresStorage(sqlDb, a.logger)
}
