package main

import (
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/timofeev.pavel.art/wallet/internal/config"
	"gitlab.com/timofeev.pavel.art/wallet/internal/lib/logs"
	"gitlab.com/timofeev.pavel.art/wallet/run"
)

func main() {
	err := godotenv.Load()

	conf := config.NewJobConf()

	logger := logs.NewLogger(conf.Logger.Level, os.Stdout)
	if err != nil {
		logger.Fatal("error loading .env file")
	}

	job := run.NewJob(conf, logger)

	exitCode := job.Bootstrap().Run()
	os.Exit(exitCode)
}
